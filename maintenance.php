<?php include "session_cek.php" ?>
<html>
<?php include "header_admin.php"; ?>

    <!-- Main Wrapper -->
    <section id="content_wrapper">

        <!-- Content -->
        <section id="content" class="animated fadeIn">

            <div class="row">
                <div class="col-md-12 mb40">

                    <div class="tab-block">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab1" data-toggle="tab">Service</a>
                            </li>
                            <li>
                                <a href="#tab2" data-toggle="tab">Cleaning</a>
                            </li>
							<li>
                                <a href="#tab3" data-toggle="tab">Selesai</a>
                            </li>
                        </ul>
                        <div class="tab-content p30">
                            <div id="tab1" class="tab-pane active">
                    <div class="panel">
                        <div class="panel-heading">
                            <span class="panel-title">Service</span>
                        </div>
                        <div class="panel-body">
						<div class="table-responsive">
                            <div class="bs-component">
                                <table class="table table-striped btn-gradient-grey">
								<thead>
                                    <tr>
                                        <th>No Order</th>
                                        <th>Nopol Kendaraan</th>
                                        <th>Merk Kendaraan</th>
                                        <th>Tanggal Masuk</th>
                                        <th>Tanggal Keluar</th>
                                        <th>Status Maintenance</th>
                                        <th>Edit</th>
                                        <th>Verifikasi</th>
                                    </tr>
                                    </thead>
                                    <tbody>
									<?php
									$fid_perusahaan=$_SESSION['fid_perusahaan'];
									$i=0;
									include "db.php";
									$q = mysqli_query($koneksi,"select * from data_maintenance,kendaraan,merk_kendaraan
									WHERE data_maintenance.fid_kendaraan=kendaraan.id_kendaraan
									AND kendaraan.fid_merk=merk_kendaraan.id_merk
									AND data_maintenance.status_maintenance=1
									AND data_maintenance.fid_perusahaan='$fid_perusahaan'"); 
									while ($row = mysqli_fetch_array($q))
									{
									?>
									 <tr>
                                        <td><?php echo $row['id_maintenance']; ?></td>
                                        <td><?php echo $row['nopol_kendaraan']; ?></td>
                                        <td><?php echo $row['merk_kendaraan']; ?></td>
                                        <td><?php echo $row['tanggal_masuk']; ?></td>
                                        <td><?php echo $row['tanggal_keluar']; ?></td>
                                        <td>Service</td>
										<td><a href="edit_service.php?id=<?php echo $row['id_maintenance'];?>"><span class="glyphicon glyphicon glyphicon-edit"></span></a></td>
										<td><a href="verifikasi_service.php?id=<?php echo $row['id_maintenance'];?>"><span class="glyphicon glyphicon glyphicon-tint"></span></a></td>
									</tr>
									<?php 
									$i++;
									}
									?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        </div>
                    </div>
                            </div>
                            <div id="tab2" class="tab-pane">
                                <div class="panel" id="spy6">
                    <div class="panel-heading">
                        <span class="panel-title pn">Cleaning</span>

                    </div>
                    <div class="panel-body pn">
                       <div class="table-responsive">
                            <div class="bs-component">
                                <table class="table table-striped btn-gradient-grey">
								<thead>
                                    <tr>
                                        <th>No Order</th>
                                        <th>Nopol Kendaraan</th>
                                        <th>Merk Kendaraan</th>
                                        <th>Tanggal Masuk</th>
                                        <th>Tanggal Keluar</th>
                                        <th>Status Maintenance</th>
                                        <th>Edit</th>
                                        <th>Verifikasi</th>
                                    </tr>
                                    </thead>
                                    <tbody>
									<?php 
									$i=0;
									include "db.php";
									$q = mysqli_query($koneksi,"select * from data_maintenance,kendaraan,merk_kendaraan
									WHERE data_maintenance.fid_kendaraan=kendaraan.id_kendaraan
									AND kendaraan.fid_merk=merk_kendaraan.id_merk
									AND data_maintenance.status_maintenance=2
									AND data_maintenance.fid_perusahaan='$fid_perusahaan'"); 
									while ($row = mysqli_fetch_array($q))
									{
									?>
									 <tr>
                                        <td><?php echo $row['id_maintenance']; ?></td>
                                        <td><?php echo $row['nopol_kendaraan']; ?></td>
                                        <td><?php echo $row['merk_kendaraan']; ?></td>
                                        <td><?php echo $row['tanggal_masuk']; ?></td>
                                        <td><?php echo $row['tanggal_keluar']; ?></td>
                                        <td>Cleaning</td>
										<td> <a href="edit_cleaning.php?id=<?php echo $row['id_maintenance'];?>"><span class="glyphicon glyphicon glyphicon-edit"></span></a></td>
										<td><a href="verifikasi_cleaning.php?id=<?php echo $row['id_maintenance'];?>"><span class="glyphicon glyphicon glyphicon-ok"></span></a></td>
                                    </tr>
									<?php 
									$i++;
									}
									?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                            </div>
							        <div id="tab3" class="tab-pane">
                                <div class="panel" id="spy6">
                    <div class="panel-heading">
                        <span class="panel-title pn">Selesai</span>

                    </div>
                    <div class="panel-body pn">
                        <div class="table-responsive">
                            <div class="bs-component">
                                <table class="table table-striped btn-gradient-grey">
								<thead>
                                    <tr>
                                        <th>No Order</th>
                                        <th>Nopol Kendaraan</th>
                                        <th>Merk Kendaraan</th>
                                        <th>Tanggal Masuk</th>
                                        <th>Tanggal Keluar</th>
                                        <th>Status Maintenance</th>
                                    </tr>
                                    </thead>
                                     <tbody>
									<?php 
									$i=0;
									include "db.php";
									$q = mysqli_query($koneksi,"select * from data_maintenance,kendaraan,merk_kendaraan
									WHERE data_maintenance.fid_kendaraan=kendaraan.id_kendaraan
									AND kendaraan.fid_merk=merk_kendaraan.id_merk
									AND data_maintenance.status_maintenance=3
									AND data_maintenance.fid_perusahaan='$fid_perusahaan'"); 
									while ($row = mysqli_fetch_array($q))
									{
									?>
									 <tr>
                                        <td><?php echo $row['id_maintenance']; ?></td>
                                        <td><?php echo $row['nopol_kendaraan']; ?></td>
                                        <td><?php echo $row['merk_kendaraan']; ?></td>
                                        <td><?php echo $row['tanggal_masuk']; ?></td>
                                        <td><?php echo $row['tanggal_keluar']; ?></td>
                                        <td>Selesai</td>
                                    </tr>
									<?php 
									$i++;
									}
									?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">

                </div>
            </div>

        </section>
        <!-- /Content -->

    </section>


</div>
<!-- /Body Wrap  -->


<!-- Scripts -->

<!-- jQuery -->
<script src="assets/js/jquery/jquery-1.12.3.min.js"></script>
<script src="assets/js/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- AnimatedSVGIcons -->
<script src="assets/fonts/animatedsvgicons/js/snap.svg-min.js"></script>
<script src="assets/fonts/animatedsvgicons/js/svgicons-config.js"></script>
<script src="assets/fonts/animatedsvgicons/js/svgicons.js"></script>
<script src="assets/fonts/animatedsvgicons/js/svgicons-init.js"></script>

<!-- HighCharts Plugin -->
<script src="assets/js/plugins/highcharts/highcharts.js"></script>

<!-- Scroll -->
<script src="assets/js/utility/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.concat.min.js"></script>

<!-- Theme Scripts -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script src="assets/js/demo/widgets_sidebar.js"></script>
<script src="assets/js/pages/dashboard_init.js"></script>


<!-- /Scripts -->

</body>


<!-- Mirrored from admink.themerex.net/basic-profile.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 05 Jul 2017 05:08:47 GMT -->
</html>