<?php include "session_cek.php" ?>
<html>
<?php include "header_admin.php"; ?>
    <!-- Daterange CSS -->
    <link rel="stylesheet" type="text/css" href="assets/js/plugins/daterange/daterangepicker.css">

    <!-- Tagmanager CSS -->
    <link rel="stylesheet" type="text/css" href="assets/js/plugins/tagmanager/tagmanager.css">

    <!-- Datetimepicker CSS -->
    <link rel="stylesheet" type="text/css" href="assets/js/plugins/datepicker/css/bootstrap-datetimepicker.css">
    <!-- Main Wrapper -->
    <section id="content_wrapper">

        <!-- Content -->
        <section id="content" class="animated fadeIn">

            <div class="row">
                <div class="col-md-12 mb40">

                    <div class="tab-block">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab1" data-toggle="tab">Edit Service</a>
                            </li>
                        </ul>
                        <div class="tab-content p30">
                            <div id="tab1" class="tab-pane active">
                    <div class="panel">
                        <div class="panel-heading">
                            <span class="panel-title">Edit Service</span>
                        </div>
                        <div class="panel-body">
										<?php 
						include ("db.php");
						$id=$_GET['id'];
						$sql = mysqli_query($koneksi,"select * from data_maintenance,kendaraan,merk_kendaraan
									WHERE data_maintenance.fid_kendaraan=kendaraan.id_kendaraan
									AND kendaraan.fid_merk=merk_kendaraan.id_merk
									AND data_maintenance.status_maintenance=1
									AND id_maintenance='$id'") or die("error sql");
						while($row = mysqli_fetch_assoc($sql)){
							$tanggal_masuk=$row['tanggal_masuk'];
							$tanggal_keluar=$row['tanggal_keluar'];
							$detail_service=$row['detail_service'];
							$estimasi_biaya=$row['estimasi_biaya'];
						}
						?> 
                   <form class="form-horizontal" method="post" action="update_service.php">

								<div class="form-group">
                                    <label for="inputStandard" class="col-lg-3 control-label">Tanggal Masuk</label>
                                    <div class="col-lg-8">
                                          <div class="col-lg-8">
                                        <div class="input-group date" id="datetimepicker2">
                                            <span class="input-group-addon cursor">
                                              <i class="fa fa-calendar"></i>
                                            </span>
                                            <input type="text" name="tanggal_masuk" class="form-control">
                                        </div>
                                    </div>
                                    </div>
                                </div>
								<div class="form-group">
                                    <label for="inputStandard" class="col-lg-3 control-label">Tanggal Keluar</label>
                                    <div class="col-lg-8">
                                         <div class="input-group date" id="datetimepicker3">
                                            <span class="input-group-addon cursor">
                                              <i class="fa fa-calendar"></i>
                                            </span>
                                            <input type="text" name="tanggal_keluar"  class="form-control">
                                        </div>
                                    </div>
                                </div>
								<div class="form-group">
                                    <label for="inputStandard" class="col-lg-3 control-label">Detail Service</label>
                                    <div class="col-lg-8">
                                         <div class="bs-component">
                                            <textarea name="detail_service" class="form-control" rows="3"><?php echo $detail_service; ?></textarea>
                                        </div>
                                    </div>
                                </div>
								<div class="form-group">
                                    <label for="inputStandard" class="col-lg-3 control-label">Estimasi Biaya</label>
                                    <div class="col-lg-8">
                                        <div class="bs-component">
                                            <input type="text" id="estimasi_biaya" name="estimasi_biaya" class="form-control"
                                                    value="<?php echo $estimasi_biaya; ?>">
                                        </div>
                                    </div>
                                </div>
                        <!-- /Wizard -->
						 <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>">
					<button type="submit" class="btn btn-primary">Simpan</button>
                    </form>
					<a href="maintenance.php"><button class="btn btn-primary">Back</button></a>
                    <!-- /Form -->

                <!-- /Spec Form -->
                        </div>
						
                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">

                </div>
            </div>

        </section>
        <!-- /Content -->

    </section>


</div>
<!-- /Body Wrap  -->


<!-- Scripts -->

<!-- jQuery -->
<script src="assets/js/jquery/jquery-1.12.3.min.js"></script>
<script src="assets/js/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- AnimatedSVGIcons -->
<script src="assets/fonts/animatedsvgicons/js/snap.svg-min.js"></script>
<script src="assets/fonts/animatedsvgicons/js/svgicons-config.js"></script>
<script src="assets/fonts/animatedsvgicons/js/svgicons.js"></script>
<script src="assets/fonts/animatedsvgicons/js/svgicons-init.js"></script>

<!-- HighCharts Plugin -->
<script src="assets/js/plugins/highcharts/highcharts.js"></script>

<!-- Scroll -->
<script src="assets/js/utility/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.concat.min.js"></script>

<!-- Theme Scripts -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script src="assets/js/demo/widgets_sidebar.js"></script>
<script src="assets/js/pages/dashboard_init.js"></script>

<!-- Scripts -->

<!-- jQuery -->
<script src="assets/js/jquery/jquery-1.12.3.min.js"></script>
<script src="assets/js/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- AnimatedSVGIcons -->
<script src="assets/fonts/animatedsvgicons/js/snap.svg-min.js"></script>
<script src="assets/fonts/animatedsvgicons/js/svgicons-config.js"></script>
<script src="assets/fonts/animatedsvgicons/js/svgicons.js"></script>
<script src="assets/fonts/animatedsvgicons/js/svgicons-init.js"></script>

<!-- Scroll -->
<script src="assets/js/utility/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.concat.min.js"></script>

<!-- Mixitup -->


<!-- Summernote -->



<!-- HighCharts Plugin -->
<script src="assets/js/plugins/highcharts/highcharts.js"></script>

<!-- Highlight JS -->


<!-- Date/Month - Pickers -->
<script src="assets/js/plugins/globalize/globalize.min.js"></script>
<script src="assets/js/plugins/moment/moment.min.js"></script>
<script src="assets/js/plugins/datepicker/js/bootstrap-datetimepicker.min.js"></script>


<!-- Magnific Popup Plugin -->


<!-- FullCalendar Plugin -->



<!-- Plugins -->














<!-- Google Map API -->





<!-- Jvectormap JS -->




<!-- Datatables JS -->





<!-- FooTable JS -->



<!-- Validate JS -->



<!-- BS Dual Listbox JS -->
<script src="assets/js/plugins/duallistbox/jquery.bootstrap-duallistbox.min.js"></script>

<!-- Bootstrap Maxlength JS -->
<script src="assets/js/plugins/maxlength/bootstrap-maxlength.min.js"></script>

<!-- Select2 JS -->
<script src="assets/js/plugins/select2/select2.min.js"></script>

<!-- Typeahead JS -->
<script src="assets/js/plugins/typeahead/typeahead.bundle.min.js"></script>

<!-- TagManager JS -->
<script src="assets/js/plugins/tagmanager/tagmanager.js"></script>

<!-- DateRange JS -->
<script src="assets/js/plugins/daterange/daterangepicker.min.js"></script>

<!-- BS Colorpicker JS -->
<script src="assets/js/plugins/colorpicker/js/bootstrap-colorpicker.min.js"></script>

<!-- MaskedInput JS -->
<script src="assets/js/plugins/jquerymask/jquery.maskedinput.min.js"></script>

<!-- Slick Slider JS -->


<!-- MarkDown JS -->




<!-- X-edit CSS -->



<script src="assets/js/plugins/typeahead/typeahead.bundle.min.js"></script>


<!-- Dropzone JS -->


<!-- Cropper JS -->


<!-- Zoom JS -->


<!-- Nestable JS -->


<!-- PNotify JS -->


<!-- Fancytree JSs -->







<!-- Ladda JS -->


<!-- NProgress JS -->


<!-- Countdown JS -->



<!-- CanvasBG JS -->


<!-- Theme Scripts -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script src="assets/js/demo/widgets_sidebar.js"></script>

























<script src="assets/js/pages/user-forms-additional-inputs.js"></script>

















<!-- /Scripts -->
<!-- /Scripts -->

</body>


<!-- Mirrored from admink.themerex.net/basic-profile.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 05 Jul 2017 05:08:47 GMT -->
</html>