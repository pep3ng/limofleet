<?php include "session_cek.php" ?>
<html>
<?php include "header_admin.php"; ?>

    <!-- Main Wrapper -->
    <section id="content_wrapper">

        <!-- Content -->
        <section id="content" class="animated fadeIn">

            <div class="row">
                <div class="col-md-12 mb40">

                    <div class="tab-block">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab1" data-toggle="tab">Merk Kendaraan</a>
                            </li>
                            <li>
                                <a href="#tab2" data-toggle="tab">Harga Kendaraan</a>
                            </li>
							<li>
                                <a href="#tab3" data-toggle="tab">Detail Kendaraan</a>
                            </li>
                        </ul>
                        <div class="tab-content p30">
                            <div id="tab1" class="tab-pane active">
                    <div class="panel">
                        <div class="panel-heading">
                            <span class="panel-title">Merk Kendaraan</span>
                        </div>
                        <div class="panel-body">
						<div class="table-responsive">
                            <div class="bs-component">
                                <table class="table table-striped btn-gradient-grey">
								<thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Produsen Kendaraan</th>
                                        <th>Merk Kendaraan</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                    </thead>
                                    <tbody>
									<?php 
									$fid_perusahaan=$_SESSION['fid_perusahaan'];
									$i=0;
									include "db.php";
									$q = mysqli_query($koneksi,"select * from merk_kendaraan where fid_perusahaan='$fid_perusahaan'"); 
									while ($row = mysqli_fetch_array($q))
									{
									?>
									 <tr>
                                        <td><?php echo $i+1; ?></td>
                                        <td><?php echo $row['produsen_kendaraan']; ?></td>
                                        <td><?php echo $row['merk_kendaraan']; ?></td>
										<td><a href="edit_merk_kendaraan.php?id=<?php echo $row['id_merk'];?>"><span class="glyphicon glyphicon glyphicon-edit"></span></a></td>
										<td><a href="delete_merk_kendaraan.php?id=<?php echo $row['id_merk'];?>"><span class="glyphicon glyphicon glyphicon-remove"></span></a></td>	
									</tr>
									<?php 
									$i++;
									}
									?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        </div>
						<a href="tambah_merk_kendaraan.php"><button class="btn btn-primary">Tambah</button></a>
                    </div>
                            </div>
                            <div id="tab2" class="tab-pane">
                                <div class="panel" id="spy6">
                    <div class="panel-heading">
                        <span class="panel-title pn">Daftar Harga Kendaraan</span>

                    </div>
                    <div class="panel-body pn">
                       <div class="table-responsive">
                            <div class="bs-component">
                                <table class="table table-striped btn-gradient-grey">
								<thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Merk Kendaraan</th>
                                        <th>Harga 12 Jam</th>
                                        <th>Harga 24 Jam</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                    </thead>
                                    <tbody>
									<?php 
									$i=0;
									include "db.php";
									$q = mysqli_query($koneksi,"select * from merk_kendaraan,harga_sewa where harga_sewa.fid_perusahaan='$fid_perusahaan' AND id_merk=fid_merk "); 
									while ($row = mysqli_fetch_array($q))
									{
									?>
									 <tr>
                                        <td><?php echo $i+1; ?></td>
                                        <td><?php echo $row['merk_kendaraan']; ?></td>
                                        <td><?php echo $row['harga_sewa12']; ?></td>
                                        <td><?php echo $row['harga_sewa24']; ?></td>
										<td><a href="edit_harga_sewa.php?id=<?php echo $row['id_harga_sewa'];?>"><span class="glyphicon glyphicon glyphicon-edit"></span></a></td>
										<td><a href="delete_harga_sewa.php?id=<?php echo $row['id_harga_sewa'];?>"><span class="glyphicon glyphicon glyphicon-remove"></span></a></td>
                                    </tr>
									<?php 
									$i++;
									}
									?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
						<a href="tambah_harga_sewa.php"><button class="btn btn-primary">Tambah</button></a>
                    </div>
                </div>
                            </div>
							        <div id="tab3" class="tab-pane">
                                <div class="panel" id="spy6">
                    <div class="panel-heading">
                        <span class="panel-title pn">Detail Kendaraan</span>

                    </div>
                    <div class="panel-body pn">
                       <div class="table-responsive">
                            <div class="bs-component">
                                <table class="table table-striped btn-gradient-grey">
								<thead>
                                    <tr>
										<th>No</th>
										<th>Nopol Kendaraan</th>
										<th>Nama Pemilik</th>
										<th>Alamat Pemilik</th>
										<th>Merk Kendaraan</th>
										<th>Tanggal Investasi</th>
										<th>Status</th>
										<th>Edit</th>
										<th>Delete</th>
                                    </tr>
                                    </thead>
                                    <tbody>
									<?php 
									$i=0;
									include "db.php";
									$q = mysqli_query($koneksi,"select * from merk_kendaraan,kendaraan where kendaraan.fid_perusahaan='$fid_perusahaan' AND id_merk=fid_merk"); 
									while ($row = mysqli_fetch_array($q))
									{
									?>
									<tr>
								<td><?php echo $i+1; ?></td>
								<td><?php echo $row['nopol_kendaraan']; ?></td>
								<td><?php echo $row['nama_pemilik']; ?></td>
								<td><?php echo $row['alamat_pemilik']; ?></td>
								<td><?php echo $row['merk_kendaraan']; ?></td>
								<td><?php echo $row['tanggal_invest']; ?></td>
								<td><?php
								if($row['status_kendaraan']==1){
								echo "Mobil Ready";
								}
								elseif($row['status_kendaraan']==2){
								echo "Mobil Disewa";
								}
								elseif($row['status_kendaraan']==3){
								echo "Mobil Pending";
								}
								elseif($row['status_kendaraan']==4){
								echo "Mobil Diservice";
								}
								else{
								echo "Mobil Dibersihkan";
								}
								?></td>
								<td><a href="edit_kendaraan.php?id=<?php echo $row['id_kendaraan'];?>"><span class="glyphicon glyphicon glyphicon-edit"></span></a></td>
								<td><a href="delete_kendaraan.php?id=<?php echo $row['id_kendaraan'];?>"><span class="glyphicon glyphicon glyphicon-remove"></span></a></td>
							</tr>
									<?php 
									$i++;
									}
									?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
						<a href="tambah_kendaraan.php"><button class="btn btn-primary">Tambah</button></a>
                    </div>
                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">

                </div>
            </div>

        </section>
        <!-- /Content -->

    </section>


</div>
<!-- /Body Wrap  -->


<!-- Scripts -->

<!-- jQuery -->
<script src="assets/js/jquery/jquery-1.12.3.min.js"></script>
<script src="assets/js/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- AnimatedSVGIcons -->
<script src="assets/fonts/animatedsvgicons/js/snap.svg-min.js"></script>
<script src="assets/fonts/animatedsvgicons/js/svgicons-config.js"></script>
<script src="assets/fonts/animatedsvgicons/js/svgicons.js"></script>
<script src="assets/fonts/animatedsvgicons/js/svgicons-init.js"></script>

<!-- HighCharts Plugin -->
<script src="assets/js/plugins/highcharts/highcharts.js"></script>

<!-- Scroll -->
<script src="assets/js/utility/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.concat.min.js"></script>

<!-- Theme Scripts -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script src="assets/js/demo/widgets_sidebar.js"></script>
<script src="assets/js/pages/dashboard_init.js"></script>


<!-- /Scripts -->

</body>


<!-- Mirrored from admink.themerex.net/basic-profile.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 05 Jul 2017 05:08:47 GMT -->
</html>