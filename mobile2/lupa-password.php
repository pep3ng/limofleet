<!DOCTYPE html>
<html lang="en-us" id="extr-page">
	<head>
		<meta charset="utf-8">
		<title> LimoIndoFleet</title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		
		<link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="css/font-awesome.min.css">
		<style>
			body{
				background: url(img/back-min.png) no-repeat center bottom fixed; 
				-webkit-background-size: cover;
				-moz-background-size: cover;
				-o-background-size: cover;
				background-size: cover;			
			}
			input::-webkit-input-placeholder {
				color: #fff;
			}

			input.line {
				border: none;
				border-bottom: 1px solid #A0A0A0;
				background: transparent;
				margin-bottom: 10px;
				display: block;
				width: 100%;
			}

			a.no-style {
				text-decoration: none;
			}

			.bg-login {
				background-color: #be80ff;
			}

			.f-white {
				color: #fff;
			}

			.t10 {
				margin-top: 10%;
			}

			.b10 {
				margin-bottom: 10%;
			}			
		</style>
		
	</head>
	<body>
	
		<div class="container">
			<div class="row" style="margin-top:17px;">
				<div class="col-md-5 col-xs-12">
					<div align="center">
						<img src="img/logo-grey.png" height="40"> 
					</div>
				</div>
			</div>
			<br/>
			<br/>
			<div align="center" style="color:#A0A0A0;">Gunakan No. Handphone untuk me-Reset Password</div>
			<br/>
			<form name="login" method="post" action="proses-login.php" enctype="multipart/form-data">
				<div class="row">
					<div class="col-xs-1">
						<i class="fa fa-2x fa-phone" style="color:#A0A0A0;"></i>
					</div>
					<div class="col-xs-11">
						<input type="text" class="line" id="cek" name="handphone" placeholder="No. Handphone" class="form-control" />
					</div>
				</div>				
				<div class="row" style="margin-top:23px;">
					<div class="col-xs-12">
						<input type="submit" id="add_data" value="RESET" disabled="disabled" class="form-control btn btn-primary" style="height:40px; color:#404040; font-size:18px;" />
					</div>
				</div>				
			</form>
			<br>
			<div align="center">
				<span style="font-size:14px; color:#A0A0A0;">Remember Password?</span> 
				<a href="login.php" style="color:#4CFF00; font-size:14px;">LOGIN</a>
			</div>
		</div>

		<nav class="navbar-fixed-bottom" style="height:37px; background-color:#E5322C;">
			<div class="text-center" style="margin-top:8px;">
				<span style="color:#fff; font-size:11px;">
					<span style="font-size:14px; color:#fff;">Belum Punya Akun?</span>&nbsp;&nbsp;&nbsp;<a href="register.php" style="color:#4CFF00; font-size:14px;">DAFTAR DISINI</a>
				</span>
			</div>
		</nav>		
		
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<script src="js/bootstrap/bootstrap.min.js"></script>
		<script>
			(function() {
				$('input#cek').keyup(function() {
					var empty = false;
					$('input').each(function() {
						if ($(this).val() == '') {
							empty = true;
						}
					});

					if (empty) {
						$('#add_data').attr('disabled', 'disabled');
					} else {
						$('#add_data').removeAttr('disabled');
					}
				});
			})()
		</script>
		
	</body>
</html>