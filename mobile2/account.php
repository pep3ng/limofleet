<?php
	include"session_app.php";
	$id_c = $_SESSION['login']['id'];
	$a = mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM client WHERE id_user='".$id_c."'"));
?>
<!DOCTYPE html>
<html lang="en-us" id="extr-page">
	<head>
		<meta charset="utf-8">
		<title> LimoIndoFleet</title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		
		<link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="css/font-awesome.min.css">
		<style>
			.garisv {
				width:1px; height:37px; 
				background-color:#C0C0C0; 
				margin-left:50%; 
				margin-right:50%;
				margin-top:5px;
			}
		</style>
	</head>
	<body>
		<nav class="navbar-fixed-top" style="height:107px; background-color:#404040;">
			<div class="text-center" style="margin-top:7px;">
				<img src="img/logo-white.png" height="35">
				<div class="container" style="margin-top:17px;">
				<div class="row">
					<div class="col-xs-3">
						<a href="member.php">
						<div align="center">
							<i class="fa fa-2x fa-home" style="color:#fff;"></i><br>
							<span style="color:#fff; font-size:12px;">Home</span>
						</div>
						</a>
					</div>
					<div class="col-xs-3">
						<a href="history.php">
						<div align="center">
							<i class="fa fa-2x fa-clock-o" style="color:#fff;"></i><br>
							<span style="color:#fff; font-size:12px;">History</span>
						</div>
						</a>
					</div>
					<div class="col-xs-3">
						<a href="help.php">
						<div align="center">
							<i class="fa fa-2x fa-comments" style="color:#fff;"></i><br>
							<span style="color:#fff; font-size:12px;">Help</span>
						</div>
						</a>
					</div>
					<div class="col-xs-3">
						<a href="account.php">
						<div align="center">
							<i class="fa fa-2x fa-user" style="color:#00FF21;"></i><br>
							<span style="color:#fff; font-size:12px;">Profile</span>
						</div>
						</a>
					</div>					
				</div>
				</div>
			</div>
		</nav>
		<div class="container">
			<div class="row" style="margin-top:120px;">
				<div class="col-xs-9">
					<?php echo $a['nama_user'];?>
				</div>
				<div class="col-xs-3">
					<div align="right"><a href="edit.php?id=<?php echo $id_c;?>"><button class="btn btn-warning">edit</button></a></div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<?php echo $a['alamat_user'];?>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<?php echo $a['email_user'];?>
				</div>
			</div>
			<div class="row" style="margin-top:20px;">
				<div class="col-xs-12">
					<?php echo $a['telepon_user'];?>
				</div>
			</div>
			<hr/>
			<?php
				if($a['foto_diri']==''){
					$foto_d = 'user.png';
					$ket_d = 'Upload Foto Anda';
				}
				else{
					$foto_d = $a['foto_diri'];
					$ket_d = 'Uploaded';
				}
				if($a['foto_ktp']==''){
					$foto_k = 'user.png';
					$ket_k = 'Upload Foto KTP Anda';
				}
				else{
					$foto_k = $a['foto_ktp'];
					$ket_k = 'Uploaded';
				}
				if($a['foto_sima']==''){
					$foto_s = 'user.png';
					$ket_s = 'Upload Foto SIM A Anda';
				}
				else{
					$foto_s = $a['foto_sima'];
					$ket_s = 'Uploaded';
				}
				if($a['foto_ksk']==''){
					$foto_ks = 'user.png';
					$ket_ks = 'Upload Foto KSK Anda';
				}
				else{
					$foto_ks = $a['foto_ksk'];
					$ket_ks = 'Uploaded';
				}				
			?>
			<div class="row">
				<div class="col-xs-9">
					<label>Foto Diri (<?php echo $ket_d;?>)</label>
				</div>
				<div class="col-xs-3">
					<div align="right">
						<?php
							if($a['foto_diri']==''){
								echo"
									<a href=\"tambah-foto-d.php?id=".$id_c."\"><button class=\"btn btn-primary\">upload</button></a>
								";
							}
							else{
								echo"
									<a href=\"edit-foto-d.php?id=".$id_c."\"><button class=\"btn btn-warning\">edit</button></a>
								";								
							}
						?>						
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<img src="../assets/img/upload/<?php echo $foto_d;?>" height="200" width="200">
				</div>
			</div>
			<hr/>
			<div class="row">
				<div class="col-xs-9">
					<label>Foto KTP (<?php echo $ket_k;?>)</label>
				</div>
				<div class="col-xs-3">
					<div align="right">
						<?php
							if($a['foto_ktp']==''){
								echo"
									<a href=\"tambah-foto-k.php?id=".$id_c."\"><button class=\"btn btn-primary\">upload</button></a>
								";
							}
							else{
								echo"
									<a href=\"edit-foto-k.php?id=".$id_c."\"><button class=\"btn btn-warning\">edit</button></a>
								";								
							}
						?>						
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<img src="../assets/img/upload/<?php echo $foto_k;?>" height="200" width="100%">
				</div>
			</div>
			<hr/>
			<div class="row">
				<div class="col-xs-9">
					<label>Foto SIM A (<?php echo $ket_s;?>)</label>
				</div>
				<div class="col-xs-3">
					<div align="right">
						<?php
							if($a['foto_sima']==''){
								echo"
									<a href=\"tambah-foto-s.php?id=".$id_c."\"><button class=\"btn btn-primary\">upload</button></a>
								";
							}
							else{
								echo"
									<a href=\"edit-foto-s.php?id=".$id_c."\"><button class=\"btn btn-warning\">edit</button></a>
								";								
							}
						?>						
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<img src="../assets/img/upload/<?php echo $foto_s;?>" height="200" width="100%">
				</div>
			</div>
			<hr/>
			<div class="row">
				<div class="col-xs-9">
					<label>Foto KSK (<?php echo $ket_ks;?>)</label>
				</div>
				<div class="col-xs-3">
					<div align="right">
						<?php
							if($a['foto_ksk']==''){
								echo"
									<a href=\"tambah-foto-ks.php?id=".$id_c."\"><button class=\"btn btn-primary\">upload</button></a>
								";
							}
							else{
								echo"
									<a href=\"edit-foto-ks.php?id=".$id_c."\"><button class=\"btn btn-warning\">edit</button></a>
								";								
							}
						?>						
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<img src="../assets/img/upload/<?php echo $foto_ks;?>" height="200" width="100%">
				</div>
			</div>
			<hr/>		
			<div class="row" style="margin-top:20px;margin-bottom:20px;">
				<div class="col-xs-12">
					<a style="cursor:pointer; text-decoration:none;" onClick="location.href='?logout=1'" title="Logout">
						<button class="btn btn-primary col-xs-12">Logout</button>
					</a>
				</div>
			</div>
			
		</div>
		
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<script src="js/bootstrap/bootstrap.min.js"></script>
		
	</body>
</html>