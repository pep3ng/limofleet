<!DOCTYPE html>
<html lang="en-us" id="extr-page">
	<head>
		<meta charset="utf-8">
		<title> LimoIndoFleet</title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		
		<link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="css/font-awesome.min.css">
		<style>
			input::-webkit-input-placeholder {
				color: #fff;
			}

			input.line {
				border: none;
				border-bottom: 1px solid #A0A0A0;
				background: transparent;
				margin-bottom: 10px;
				display: block;
				width: 100%;
			}

			a.no-style {
				text-decoration: none;
			}

			.bg-login {
				background-color: #be80ff;
			}

			.f-white {
				color: #fff;
			}

			.t10 {
				margin-top: 10%;
			}

			.b10 {
				margin-bottom: 10%;
			}			
		</style>
		
	</head>
	<body>
	
		<div class="container">
			<div class="row" style="margin-top:17px;">
				<div class="col-md-5 col-xs-12">
					<div align="center">
						<img src="img/logo-grey.png" height="40"> 
					</div>
				</div>
			</div>
			<br/>
			<br/>
			<div align="center" style="color:#A0A0A0; font-size:11px;">Sebuah SMS dengan Kode Verifikasi telah dikirim ke No. Handphone +62XXXXXXXX687 associated dengan email dibawah ini.<br><span style="font-size:18px; color:#4CFF00;">syaiful.sr@gmail.com</span></div>
			<br/>
			<div align="center">
				ENTER CODE
				<br/>
				<i class="fa fa-2x fa-qrcode" style="color:#A0A0A0;"></i>
				<br/>
				<br/>
				<form name="login" id="smart-form-register" method="post" action="proses-login.php" enctype="multipart/form-data">
					<input type="text" class="line" id="cek" name="handphone" placeholder="Kode Verifikasi" class="form-control" maxlength="4" style="width:100px; text-align:center;" />
					<input type="submit" id="add_data" value="KONFIRMASI" disabled="disabled" class="form-control btn btn-primary" style="height:40px; color:#404040; font-size:18px;" />
				</form>
			</div>
			<br/>
			<div align="center">
				<span style="font-size:14px; color:#007F0E;">RESEND</span> 
			</div>
		</div>
		<nav class="navbar-fixed-bottom" style="height:207px; background-color:#E5322C;">
			<img src="img/back-min.png" width="100%">
		</nav>		
		
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<script src="js/bootstrap/bootstrap.min.js"></script>
		<script>
			(function() {
				$('input#cek').keyup(function() {
					var empty = false;
					$('input').each(function() {
						if ($(this).val() == '') {
							empty = true;
						}
					});

					if (empty) {
						$('#add_data').attr('disabled', 'disabled');
					} else {
						$('#add_data').removeAttr('disabled');
					}
				});
			})()
		</script>
		
	</body>
</html>