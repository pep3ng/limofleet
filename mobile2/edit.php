<?php
	include"session_app.php";
	$id_c = $_GET['id'];
	$a = mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM client WHERE id_user='".$id_c."'"));
?>
<!DOCTYPE html>
<html lang="en-us" id="extr-page">
	<head>
		<meta charset="utf-8">
		<title> LimoIndoFleet</title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		
		<link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="css/font-awesome.min.css">
		<style>
			.garisv {
				width:1px; height:37px; 
				background-color:#C0C0C0; 
				margin-left:50%; 
				margin-right:50%;
				margin-top:5px;
			}
		</style>
	</head>
	<body>
	
		<nav class="navbar-fixed-top" style="height:47px; background-color:#404040;">
			<div class="row" style="margin-top:10px;">
				<div class="col-xs-2">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size:18px;"><a href="account.php"><i class="fa fa-arrow-left" style="color:#fff;"></i></a></span>
				</div>
				<div class="col-xs-10">
					<span style="font-size:18px;color:#fff;">Update Data Diri</span>
				</div>
			</div>
		</nav>
		<div class="container" style="margin-top:55px;">
			<br/>
			<?php
				if(isset($_GET['info'])){
					echo $_GET['info'];
				}
			?>
			<form name="upload-bukti" method="post" action="proses-edit.php" enctype="multipart/form-data">
				<div class="row">
					<div class="col-xs-12">
						<label>Nama</label>
						<input type="text" name="userID" class="hidden" value="<?php echo $_GET['id'];?>" />
						<input type="text" name="nama" class="form-control" value="<?php echo $a['nama_user'];?>" readonly />
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-xs-12">
						<label>No. Handphone</label>
						<input type="text" name="telepon" class="form-control" value="<?php echo $a['telepon_user'];?>" readonly />
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-xs-12">
						<label>Alamat</label>
						<input type="text" name="alamat" class="form-control" value="<?php echo $a['alamat_user'];?>" required />
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-xs-12">
						<label>Email</label>
						<input type="email" name="email" class="form-control" value="<?php echo $a['email_user'];?>" required />
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-xs-12">
						<button type="submit" class="btn btn-primary form-control">Update</button>
					</div>
				</div>				
			</form>
		</div>
		
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<script src="js/bootstrap/bootstrap.min.js"></script>
		
	</body>
</html>	