<?php
	include"session_app.php";
?>
<!DOCTYPE html>
<html lang="en-us" id="extr-page">
	<head>
		<meta charset="utf-8">
		<title> LimoIndoFleet</title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		
		<link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="css/font-awesome.min.css">
		<style>
			.garisv {
				width:1px; height:37px; 
				background-color:#C0C0C0; 
				margin-left:50%; 
				margin-right:50%;
				margin-top:5px;
			}
		</style>
	</head>
	<body>
	
		<nav class="navbar-fixed-top" style="height:47px; background-color:#404040;">
			<div class="row" style="margin-top:10px;">
				<div class="col-xs-2">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size:18px;"><a href="account.php"><i class="fa fa-arrow-left" style="color:#fff;"></i></a></span>
				</div>
				<div class="col-xs-10">
					<span style="font-size:18px;color:#fff;">Upload Foto Diri</span>
				</div>
			</div>
		</nav>
		<div class="container" style="margin-top:55px;">
			<br/>
			<?php
				if(isset($_GET['info'])){
					echo $_GET['info'];
				}
			?>
			<form name="upload-bukti" method="post" action="proses-tambah-foto-d.php" enctype="multipart/form-data">
				<div class="row">
					<div class="col-xs-12">
						<input type="text" name="userID" class="hidden" value="<?php echo $_GET['id'];?>" />
						<input type="file" name="fotoDIRI" class="btn btn-default" accept="image/*" required />
						<p class="help-block">
							ukuran foto tidak boleh lebih dari 200kB.
						</p>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<button type="submit" class="btn btn-primary form-control">Upload</button>
					</div>
				</div>				
			</form>
		</div>
		
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<script src="js/bootstrap/bootstrap.min.js"></script>
		
	</body>
</html>	