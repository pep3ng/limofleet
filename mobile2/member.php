<?php
	include"session_app.php";
	$id_c = $_SESSION['login']['id'];
	$a = mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM client WHERE id_user='".$id_c."'"));
?>
<!DOCTYPE html>
<html lang="en-us" id="extr-page">
	<head>
		<meta charset="utf-8">
		<title> LimoIndoFleet</title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		
		<link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="css/font-awesome.min.css">
		<!-- datepicker css -->
		<link href="jquery-ui-1.11.4/smoothness/jquery-ui.css" rel="stylesheet" />
		<link href="jquery-ui-1.11.4/jquery-ui.theme.css" rel="stylesheet" />
		
		<style>
			.garisv {
				width:1px; height:37px; 
				background-color:#C0C0C0; 
				margin-left:50%; 
				margin-right:50%;
				margin-top:5px;
			}
		</style>
	</head>
	<body>
		<nav class="navbar-fixed-top" style="height:107px; background-color:#404040;">
			<div class="text-center" style="margin-top:7px;">
				<img src="img/logo-white.png" height="35">
				<div class="container" style="margin-top:17px;">
				<div class="row">
					<div class="col-xs-3">
						<a href="member.php">
						<div align="center">
							<i class="fa fa-2x fa-home" style="color:#00FF21;"></i><br>
							<span style="color:#fff; font-size:12px;">Home</span>
						</div>
						</a>
					</div>
					<div class="col-xs-3">
						<a href="history.php">
						<div align="center">
							<i class="fa fa-2x fa-clock-o" style="color:#fff;"></i><br>
							<span style="color:#fff; font-size:12px;">History</span>
						</div>
						</a>
					</div>
					<div class="col-xs-3">
						<a href="help.php">
						<div align="center">
							<i class="fa fa-2x fa-comments" style="color:#fff;"></i><br>
							<span style="color:#fff; font-size:12px;">Help</span>
						</div>
						</a>
					</div>
					<div class="col-xs-3">
						<a href="account.php">
						<div align="center">
							<i class="fa fa-2x fa-user" style="color:#fff;"></i><br>
							<span style="color:#fff; font-size:12px;">Profile</span>
						</div>
						</a>
					</div>					
				</div>
				</div>
			</div>
			<div style="background-color:#F0F0F0; margin-top:0px; height:47px;">
				<div class="row">
					<div class="col-xs-5">
						<div align="center" style="margin-top:5px;">
							<img src="img/wallet.png" height="20" width="100">
							<br/>
							<span style="font-size:12px;">Rp. 0,-</span>
						</div>
					</div>
					<div class="col-xs-2">
						<div class="garisv"></div>
					</div>				
					<div class="col-xs-5">
						<div align="center" style="margin-top:5px;">
							<img src="img/poin.png" height="20" width="100">
							<br/>
							<span style="font-size:12px;">0 Points</span>
						</div>
					</div>			
				</div>
			</div>	
		</nav>	
		<div id="map" style="width:100%;height:200px;margin-top:152px;"></div>				
		<br/>
		<div class="container">	
				<?php
					if(isset($_GET['info'])){
						echo $_GET['info'];
					}
				?>
				<?php
					if($a['foto_diri']=='' OR $a['foto_ktp']=='' OR $a['foto_sima']=='' OR $a['foto_ksk']==''){
						echo"
						".$a['nama_user']." Tolong Upload dulu Foto Diri, Foto KTP, Foto SIM A dan Foto KSK sebelum melakukan ORDER RENT CAR.
						<br/>
						Formulir RENT CAR belum aktif bila ".$a['nama_user']." belum meng-upload foto data diri, terima kasih.";
					}
					else{
				?>
				<form name="login" id="myform" method="post" action="simpan-order.php" enctype="multipart/form-data">
					<div class="row">
						<div class="col-md-5 col-xs-12">
							<input type="text" name="txtidclient" value="<?php echo $id_c;?>" class="hidden" />
							<input type="text" name="lati" id="lati"  class="hidden" />
							<input type="text" name="longi" id="longi" class="hidden" />
							<label>Pilih Vendor</label>
							<select class="form-control" name="txtperusahaan" id="rekanan" required>
								<option></option>
								<?php
									$dat = "SELECT * FROM perusahaan ORDER BY id_perusahaan ASC";
									$que = mysqli_query($conn,$dat);
									while($hsl=mysqli_fetch_array($que)){
										echo"
											<option value=\"".$hsl['id_perusahaan']."\">".$hsl['perusahaan']."</option>
										";
									}
								?>
							</select>											
						</div>
					</div>
					<br/>
					<div class="row">
						<div class="col-md-5 col-xs-12">
							<label>Pilih Kendaraan</label>
							<select class="form-control" name="txtkendaraan" id="kendaraan" required>
								<option></option>
								<?php
									$dat = "SELECT * FROM kendaraan WHERE status_kendaraan='1' ORDER BY id_kendaraan ASC";
									$que = mysqli_query($conn,$dat);
									while($hsl=mysqli_fetch_array($que)){
										$i = mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM merk_kendaraan WHERE id_merk='".$hsl['fid_merk']."'"));
										echo"
											<option value=\"".$hsl['id_kendaraan']."\">".$hsl['nopol_kendaraan']." | ".$i['produsen_kendaraan']." ".$i['merk_kendaraan']."</option>
										";
									}
								?>
							</select>											
						</div>
					</div>
					<br/>	
					<div class="row">					
						<div class="col-md-5 col-xs-12">
							<label>Jenis Sewa</label>
							<select class="form-control" name="txtjnssewa" id="jenissewa" required />
								<option></option>
								<option value="1">Dengan Driver</option>
								<option value="2">Lepas Kunci</option>
							</select>
						</div>
					</div>
					<br/>				
					<div id="jaminan"></div>	
					<div class="row">					
						<div class="col-md-5 col-xs-12">
							<label>Tanggal Sewa</label>
							<input type="text" name="txttgl1" class="form-control" id="tanggal" required /><br/>
						</div>
					</div>
					<div class="row">					
						<div class="col-md-5 col-xs-12">
							<label>Tanggal Kembali</label>
							<input type="text" name="txttgl2" class="form-control" id="tanggal1" required /><br/>
						</div>
					</div>
					<div class="row">					
						<div class="col-md-5 col-xs-12">
							<label>Tujuan</label>
							<input type="text" name="tujuan" class="form-control" required /><br/>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-5 col-xs-12">
							<input type="submit" name="submit" value="Order Sekarang" class="form-control btn btn-primary" />
						</div>
					</div>
				</form>
				<br/>
				<br/>
				<?php
					}
				?>
		</div>
		
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<script src="js/bootstrap/bootstrap.min.js"></script>
		<!-- datepicker -->
		<script src="jquery-ui-1.11.4/jquery-ui.js"></script>
		<script src="jquery-ui-1.11.4/jquery-ui.min.js"></script>
		<!-- datepicker -->
		<script>
			$(document).ready(function(){
				$("#tanggal").datepicker({
				})
			})
			$(document).ready(function(){
				$("#tanggal1").datepicker({
				})
			})
		</script>
		
		<script>
		var map, infoWindow;
		function initMap() {
			map = new google.maps.Map(document.getElementById('map'), {
			center: {lat: -7.3088459, lng: 112.7556214},
			zoom: 15
			});
			infoWindow = new google.maps.InfoWindow;

			// Try HTML5 geolocation.
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(function(position) {
					var pos = {
						lat: position.coords.latitude,
						lng: position.coords.longitude
					};
					
					document.getElementById('lati').value = position.coords.latitude;
					document.getElementById('longi').value = position.coords.longitude;
					
					var marker = new google.maps.Marker({
						position: pos,
						animation:google.maps.Animation.BOUNCE,
						icon: 'img/client.png',
						map: map
					});

					infoWindow.setPosition(pos);
					infoWindow.open(map,marker);
					map.setCenter(pos);
				}, function() {
					handleLocationError(true, infoWindow, map.getCenter());
				});
			} else {
			// Browser doesn't support Geolocation
			handleLocationError(false, infoWindow, map.getCenter());
			}
		}

		function handleLocationError(browserHasGeolocation, infoWindow, pos) {
			infoWindow.setPosition(pos);
			infoWindow.setContent(browserHasGeolocation ?
                              'Error: Aktifkan Permission Get Location Info di Setting.' :
                              'Error: Smartphone anda tidak mendukung aplikasi ini.');
			infoWindow.open(map);
		}
		</script>
		<script async defer
			src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDn-LLwACxbTzOTts4GlSc-EhEQ41k-9lw&callback=initMap">
		</script>
		
		<script>
			(function() {
				$('input#cek').keyup(function() {
					var empty = false;
					$('input').each(function() {
						if ($(this).val() == '') {
							empty = true;
						}
					});

					if (empty) {
						$('#add_data').attr('disabled', 'disabled');
					} else {
						$('#add_data').removeAttr('disabled');
					}
				});
			})()
		</script>

		<script type="text/javascript">
			var htmlobjek;
			$(document).ready(function(){
				//apabila terjadi event onchange terhadap object <select id=propinsi>
				$("#rekanan").change(function(){
					var rekanan = $("#rekanan").val();
					$.ajax({
						url: "kendaraan.php",
						data: "rekanan="+rekanan,
						cache: false,
						success: function(msg){
						//jika data sukses diambil dari server kita tampilkan
						//di <select id=kota>
						$("#kendaraan").html(msg);
						}
					});
				});
			});
		</script>			

		<script type="text/javascript">
			var htmlobjek;
			$(document).ready(function(){
				//apabila terjadi event onchange terhadap object <select id=propinsi>
				$("#jenissewa").change(function(){
					var jenissewa = $("#jenissewa").val();
					$.ajax({
						url: "jaminan.php",
						data: "jenissewa="+jenissewa,
						cache: false,
						success: function(msg){
						//jika data sukses diambil dari server kita tampilkan
						//di <select id=kota>
						$("#jaminan").html(msg);
						}
					});
				});
			});
		</script>			
		
	</body>
</html>