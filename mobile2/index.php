<!DOCTYPE html>
<html lang="en-us" id="extr-page">
	<head>
		<meta charset="utf-8">
		<title> LimoIndoFleet</title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		
		<link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="css/font-awesome.min.css">
	</head>
	<body>
	
		<div class="container">
			<div class="row" style="margin-top:17px;">
				<div class="col-md-5 col-xs-12">
					<div align="center">
						<img src="img/logo-grey.png" height="40"> 
					</div>
				</div>
			</div>
			<br/>
			<br/>
			<br/>
			<div class="row">
				<div class="col-md-5 col-xs-12">
					<div align="center">
						<img src="img/loading.gif" width="70%">
						<br/>
						<span style="color:#404040;">LIMOFLEET LOADING...</span>
					</div>
				</div>
			</div>
		</div>
		<nav class="navbar-fixed-bottom" style="height:207px; background-color:#E5322C;">
			<img src="img/back-min.png" width="100%">
			<div align="center" style="margin-top:8px;color:#fff;">&copy; copyright 2017</div>
		</nav>		
		
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<script src="js/bootstrap/bootstrap.min.js"></script>

		<script language=javascript>
			setTimeout("location.href='login.php'", 4000);
		</script>
	</body>
</html>