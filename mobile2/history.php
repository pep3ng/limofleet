<?php
	include"session_app.php";
	$id_c = $_SESSION['login']['id'];
	$a = mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM client WHERE id_user='".$id_c."'"));
?>
<!DOCTYPE html>
<html lang="en-us" id="extr-page">
	<head>
		<meta charset="utf-8">
		<title> LimoIndoFleet</title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		
		<link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="css/font-awesome.min.css">
		<style>
			.garisv {
				width:1px; height:37px; 
				background-color:#C0C0C0; 
				margin-left:50%; 
				margin-right:50%;
				margin-top:5px;
			}
		</style>
	</head>
	<body>
		<nav class="navbar-fixed-top" style="height:107px; background-color:#404040;">
			<div class="text-center" style="margin-top:7px;">
				<img src="img/logo-white.png" height="35">
				<div class="container" style="margin-top:17px;">
				<div class="row">
					<div class="col-xs-3">
						<a href="member.php">
						<div align="center">
							<i class="fa fa-2x fa-home" style="color:#fff;"></i><br>
							<span style="color:#fff; font-size:12px;">Home</span>
						</div>
						</a>
					</div>
					<div class="col-xs-3">
						<a href="history.php">
						<div align="center">
							<i class="fa fa-2x fa-clock-o" style="color:#00FF21;"></i><br>
							<span style="color:#fff; font-size:12px;">History</span>
						</div>
						</a>
					</div>
					<div class="col-xs-3">
						<a href="help.php">
						<div align="center">
							<i class="fa fa-2x fa-comments" style="color:#fff;"></i><br>
							<span style="color:#fff; font-size:12px;">Help</span>
						</div>
						</a>
					</div>
					<div class="col-xs-3">
						<a href="account.php">
						<div align="center">
							<i class="fa fa-2x fa-user" style="color:#fff;"></i><br>
							<span style="color:#fff; font-size:12px;">Profile</span>
						</div>
						</a>
					</div>					
				</div>
				</div>
			</div>
			<ul class="nav nav-tabs" style="margin-top:3px;background-color:#fff;">
				<li class="active" style="width:50%"><a data-toggle="tab" href="#progress"><i class="fa fa-clock-o"></i> In Progress</a></li>
				<li style="width:50%"><a data-toggle="tab" href="#complete"><i class="fa fa-check"></i> Completed</a></li>
			</ul>			
		</nav>
		<div class="tab-content">
			<div id="progress" class="tab-pane fade in active" style="margin-top:127px;padding:17px;">
				<?php
					$cari = mysqli_num_rows(mysqli_query($conn,"SELECT * FROM data_order WHERE fid_user='".$id_c."' AND status_order BETWEEN '1' AND '2'"));
					if($cari<1){
						$b = mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM client WHERE id_user='".$id_c."'"));
						echo"
							<div align=\"center\"><br>Kelihatannya Belum ada order dari ".$b['nama_user']."<br>Gunakan menu <a href=\"member.php\">Home</a> untuk Order Rental.</div>
						";
					}
					else{
				?>						
				<table class="table table-hover" width="100%">
					<thead>
						<tr>
							<th width="70%">Info Order</th>
							<th width="30%">Status</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$dat = "SELECT * FROM data_order WHERE fid_user='".$id_c."' AND status_order BETWEEN '1' AND '2' ORDER BY id_order DESC";
							$que = mysqli_query($conn,$dat);
							while($c=mysqli_fetch_array($que)){	
								$tgl_b = implode('',explode('-',$c['tanggal_berangkat']));
								$tgl_k = implode('',explode('-',$c['tanggal_datang']));
								if($c['status_order']=='1'){$status = 'Pending';}
								elseif($c['status_order']=='2'){$status = 'Confirm';}
								elseif($c['status_order']=='3'){$status = 'Selesai';}
								$d = mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM perusahaan WHERE id_perusahaan='".$c['fid_perusahaan']."'"));
								$e = mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM kendaraan WHERE id_kendaraan='".$c['fid_kendaraan']."'"));
								$f = mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM merk_kendaraan WHERE id_merk='".$e['fid_merk']."'"));
						?>
						<tr>
							<td width="70%">
								Order ID: <?php echo $c['id_order'];?><br/>
								Vendor Rental: <?php echo $d['perusahaan'];?><br/>
								Alamat Rental: <?php echo $d['alamat_perusahaan'];?><br/>
								Mobil: <?php echo $f['produsen_kendaraan'];?> <?php echo $f['merk_kendaraan'];?><br/>
								Nopol: <?php echo $e['nopol_kendaraan'];?><br/>
								Tgl Berangkat: <?php echo substr($tgl_b,6,2)."-".substr($tgl_b,4,2)."-".substr($tgl_b,0,4);?><br/>
								Jam Berangkat: <?php echo $c['jam_berangkat'];?><br/>
								Tgl Kembali: <?php echo substr($tgl_k,6,2)."-".substr($tgl_k,4,2)."-".substr($tgl_k,0,4);?><br/>
								Jam Kembali: <?php echo $c['jam_datang'];?><br/>
								KM Awal: <?php echo $c['km_awal'];?><br/>
								KM Akhir: <?php echo $c['km_akhir'];?><br/>
								Jaminan: <?php echo $c['jenis_jaminan'];?><br/>
								Ket Jaminan: <?php echo $c['info_jaminan'];?><br/>
								Tujuan: <?php echo $c['tujuan'];?><br/>
							</td>
							<td width="30%">
								<?php
									if($c['status_order']==1){
										echo"<button class=\"btn btn-warning\">".$status."</button>";
									}
									else{
										if($c['bukti_transfer']==''){
											echo"
												<button class=\"btn btn-primary\">".$status."</button>
												<br/><br/>
												<a href=\"upload-bukti.php?id=".$c['id_order']."\"><button class=\"btn btn-primary\"><i class=\"fa fa-money\"></i><br/>Upload Bukti Transfer</button></a>
											";
										}
										else{
											echo"
												<button class=\"btn btn-primary\">".$status."</button>
												<br/><br/>
												<a href=\"preview-bukti.php?id=".$c['id_order']."\"><img src=\"../assets/img/upload/".$c['bukti_transfer']."\" width=\"140\" height=\"200\"></a>
											";
										}
									}
								?>
							</td>
						<?php
							}
						?>
						</tr>
					</tbody>
				</table>
				<?php
					}
				?>
			</div>
			<div id="complete" class="tab-pane fade in" style="margin-top:129px;padding:17px;">
				<?php
					$cari = mysqli_num_rows(mysqli_query($conn,"SELECT * FROM data_order WHERE fid_user='".$id_c."' AND status_order='3'"));
					if($cari<1){
						$c = mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM client WHERE id_user='".$id_c."'"));
						echo"
							<div align=\"center\"><br>Kelihatannya Belum ada order yang selesai<br>".$c['nama_user']."<br>Gunakan menu <a href=\"member.php\">Home</a> untuk Order Rental.</div>
						";
					}
					else{
				?>			
				<table class="table table-hover" width="100%">
					<thead>
						<tr>
							<th width="70%">Info Order</th>
							<th width="30%">Status</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$dat = "SELECT * FROM data_order WHERE fid_user='".$id_c."' AND status_order='3' ORDER BY id_order DESC";
							$que = mysqli_query($conn,$dat);
							while($c=mysqli_fetch_array($que)){	
								$tgl_b = implode('',explode('-',$c['tanggal_berangkat']));
								$tgl_k = implode('',explode('-',$c['tanggal_datang']));
								if($c['status_order']=='1'){$status = 'Pending';}
								elseif($c['status_order']=='2'){$status = 'Aktif';}
								elseif($c['status_order']=='3'){$status = 'Selesai';}
								$d = mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM perusahaan WHERE id_perusahaan='".$c['fid_perusahaan']."'"));
								$e = mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM kendaraan WHERE id_kendaraan='".$c['fid_kendaraan']."'"));
								$f = mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM merk_kendaraan WHERE id_merk='".$e['fid_merk']."'"));
						?>
						<tr>
							<td width="70%">
								Order ID: <?php echo $c['id_order'];?><br/>
								Vendor Rental: <?php echo $d['perusahaan'];?><br/>
								Alamat Rental: <?php echo $d['alamat_perusahaan'];?><br/>
								Mobil: <?php echo $f['produsen_kendaraan'];?> <?php echo $f['merk_kendaraan'];?><br/>
								Nopol: <?php echo $e['nopol_kendaraan'];?><br/>
								Tgl Berangkat: <?php echo substr($tgl_b,6,2)."-".substr($tgl_b,4,2)."-".substr($tgl_b,0,4);?><br/>
								Jam Berangkat: <?php echo $c['jam_berangkat'];?><br/>
								Tgl Kembali: <?php echo substr($tgl_k,6,2)."-".substr($tgl_k,4,2)."-".substr($tgl_k,0,4);?><br/>
								Jam Kembali: <?php echo $c['jam_datang'];?><br/>
								KM Awal: <?php echo $c['km_awal'];?><br/>
								KM Akhir: <?php echo $c['km_akhir'];?><br/>
								Jaminan: <?php echo $c['jenis_jaminan'];?><br/>
								Ket Jaminan: <?php echo $c['info_jaminan'];?><br/>
								Tujuan: <?php echo $c['tujuan'];?><br/>
							</td>
							<td width="30%">
								<?php
									if($c['status_order']==1){
										echo"<button class=\"btn btn-warning\">".$status."</button>";
									}
									else{
										echo"<button class=\"btn btn-success\">".$status."</button>";
									}
								?>
								</td>
						<?php
							}
						?>
						</tr>
					</tbody>
				</table>
				<?php
					}
				?>				
			</div>	
		</div>
		
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<script src="js/bootstrap/bootstrap.min.js"></script>
		
	</body>
</html>