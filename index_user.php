<?php include "session_cek.php" ?>
<html>
<?php include "header_user.php"; ?>

    <!-- Main Wrapper -->
    <section id="content_wrapper">

        <!-- Content -->
        <section id="content" class="animated fadeIn">

            <div class="row">
                <div class="col-md-12 mb40">

                    <div class="tab-block">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab1" data-toggle="tab">Profile</a>
                            </li>
                            <li>
                                <a href="#tab2" data-toggle="tab">Dokumen</a>
                            </li>
                        </ul>
                        <div class="tab-content p30">
                            <div id="tab1" class="tab-pane active">

                    <?php
						$fid_user=$_SESSION['fid_user'];
						include ("db.php");
						$sql = mysqli_query($koneksi,"SELECT * FROM client where id_user='$fid_user'") or die("error sql");
						while($row = mysqli_fetch_assoc($sql)){
							$nama_user=$row['nama_user'];
							$alamat_user=$row['alamat_user'];
							$telepon_user=$row['telepon_user'];
							$email_user=$row['email_user'];
							$foto_diri=$row['foto_diri'];
							$foto_ktp=$row['foto_ktp'];
							$foto_sima=$row['foto_sima'];
							$foto_ksk=$row['foto_ksk'];
						}
						?>
                    <div class="panel">
                        <div class="panel-heading">
                            <span class="panel-title">Profile</span>
                        </div>
                        <div class="panel-body">

                            <form class="form-horizontal">
							<div class="form-group">
                                    <label class="col-lg-3 control-label">Avatar</label>

                                    <div class="col-lg-8">
                                        <div class="bs-component">
                                            <img class="mw140 mr25 mb20" src="assets/img/upload/<?php echo $foto_diri; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Nama</label>

                                    <div class="col-lg-8">
                                        <div class="bs-component">
                                            <p class="form-control-static text-muted"><?php echo $nama_user; ?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label" for="textArea3">Alamat</label>

                                    <div class="col-lg-8">
                                        <div class="bs-component">
                                              <textarea class="form-control" id="textArea3" rows="3"
                                                        disabled><?php echo $alamat_user; ?></textarea>
                                        </div>
                                    </div>
                                </div>
								   <div class="form-group">
                                    <label class="col-lg-3 control-label">No.Telepon</label>

                                    <div class="col-lg-8">
                                        <div class="bs-component">
                                            <p class="form-control-static text-muted"><?php echo $telepon_user; ?></p>
                                        </div>
                                    </div>
                                </div>
								   <div class="form-group">
                                    <label class="col-lg-3 control-label">Alamat Email</label>

                                    <div class="col-lg-8">
                                        <div class="bs-component">
                                            <p class="form-control-static text-muted"><?php echo $email_user; ?></p>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                            </div>
                            <div id="tab2" class="tab-pane">
                                <div class="panel" id="spy6">
                    <div class="panel-heading">
                        <span class="panel-title pn">Dokumen Customer</span>

                    </div>
                    <div class="panel-body pn">
                        <div class="table-responsive">
                            <div class="bs-component">
                                <table class="table table-striped btn-gradient-grey">
                                    <tbody>
									 <tr>
                                        <td>1</td>
                                        <td>Kartu Tanda Penduduk (KTP)</td>
                                        <td><a href="assets/img/upload/<?php echo $foto_ktp; ?>" target="_blank">Lihat File</a></td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Surat Izin Mengemudi (SIM)</td>
                                        <td><a href="assets/img/upload/<?php echo $foto_sima; ?>" target="_blank">Lihat File</a></td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Kartu Susunan Keluarga</td>
                                        <td><a href="assets/img/upload/<?php echo $foto_ksk; ?>" target="_blank">Lihat File</a></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">

                </div>
            </div>

        </section>
        <!-- /Content -->

    </section>


</div>
<!-- /Body Wrap  -->


<!-- Scripts -->

<!-- jQuery -->
<script src="assets/js/jquery/jquery-1.12.3.min.js"></script>
<script src="assets/js/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- AnimatedSVGIcons -->
<script src="assets/fonts/animatedsvgicons/js/snap.svg-min.js"></script>
<script src="assets/fonts/animatedsvgicons/js/svgicons-config.js"></script>
<script src="assets/fonts/animatedsvgicons/js/svgicons.js"></script>
<script src="assets/fonts/animatedsvgicons/js/svgicons-init.js"></script>

<!-- HighCharts Plugin -->
<script src="assets/js/plugins/highcharts/highcharts.js"></script>

<!-- Scroll -->
<script src="assets/js/utility/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.concat.min.js"></script>

<!-- Theme Scripts -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script src="assets/js/demo/widgets_sidebar.js"></script>
<script src="assets/js/pages/dashboard_init.js"></script>


<!-- /Scripts -->

</body>


<!-- Mirrored from admink.themerex.net/basic-profile.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 05 Jul 2017 05:08:47 GMT -->
</html>