-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 18, 2017 at 04:22 PM
-- Server version: 5.5.55-cll
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u8726119_traccar`
--

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `id_user` int(10) NOT NULL,
  `nama_user` varchar(50) DEFAULT NULL,
  `alamat_user` text,
  `telepon_user` text,
  `email_user` text,
  `foto_diri` varchar(255) DEFAULT NULL,
  `foto_ktp` varchar(255) DEFAULT NULL,
  `foto_sima` varchar(255) DEFAULT NULL,
  `foto_ksk` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`id_user`, `nama_user`, `alamat_user`, `telepon_user`, `email_user`, `foto_diri`, `foto_ktp`, `foto_sima`, `foto_ksk`) VALUES
(1, 'Riyanto', 'Bendul Merisi Selatan IV/72', '082382381', 'riyanto@gmail.com', 'avatar.jpg', 'ktp.jpg', 'sim.jpg', 'ksk.jpg'),
(2, 'Romo Sianturi', 'Jalan Jenggolo 3 Sidoarjo', '023230230', 'romosianturi@gmail.com', '76178-foto.jpg', '23381-fotoktp.jpg', '27614-sima.jpg', '66688-fotoksk.jpg'),
(4, 'Joni', 'Buduran 5 Sidoarjo', '08123123', 'joni@gmail.com', '30740-flyer A52.jpg', '53756-flyer A5.jpg', '28289-flyer a5 3.jpg', '49047-flyer a5 4.jpg'),
(7, 'Bp. Syaiful Alamsah', 'Kalitengah Utara No.37', '081217123687', 'syaiful.sr@gmail.com', 'Kimono-laki1.jpg', 'Kimono-laki1.jpg', 'Kimono-laki1.jpg', 'Kimono-laki1.jpg'),
(10, 'Bp. Sultan Rafly', 'kalitengah utara no.37', '08178787878', 'sultan@gmail.com', 'Kimono-laki1.jpg', 'Kimono-laki1.jpg', 'Kimono-laki1.jpg', 'Kimono-laki1.jpg'),
(13, 'Ibu. cikyun nyimas', 'kalitengah utara no.37', '089567878889', 'cikyun8@gmail.com', 'Kimono-laki1.jpg', 'images.jpg', 'images.jpg', 'images.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `data_maintenance`
--

CREATE TABLE `data_maintenance` (
  `id_maintenance` int(10) NOT NULL,
  `tanggal_masuk` date DEFAULT NULL,
  `tanggal_keluar` date DEFAULT NULL,
  `fid_kendaraan` int(10) NOT NULL,
  `fid_user` int(10) NOT NULL,
  `status_maintenance` int(10) NOT NULL,
  `estimasi_biaya` double DEFAULT NULL,
  `detail_service` text,
  `detail_cleaning` text,
  `fid_perusahaan` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `data_maintenance`
--

INSERT INTO `data_maintenance` (`id_maintenance`, `tanggal_masuk`, `tanggal_keluar`, `fid_kendaraan`, `fid_user`, `status_maintenance`, `estimasi_biaya`, `detail_service`, `detail_cleaning`, `fid_perusahaan`) VALUES
(2, '0000-00-00', '0000-00-00', 1, 1, 3, 111111, 'tes', NULL, 1),
(5, NULL, NULL, 2, 0, 3, NULL, NULL, 'Dibersihan lengkap dan dicuci karpet bagian dalamnya', 1),
(6, '0000-00-00', '0000-00-00', 5, 4, 3, 1200000, 'Perbaikan Oli dan Baret', NULL, 1),
(7, NULL, NULL, 11, 7, 2, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `data_order`
--

CREATE TABLE `data_order` (
  `id_order` int(10) NOT NULL,
  `km_awal` double DEFAULT NULL,
  `km_akhir` double DEFAULT NULL,
  `tanggal_berangkat` date DEFAULT NULL,
  `jam_berangkat` time DEFAULT NULL,
  `tanggal_datang` date DEFAULT NULL,
  `jam_datang` time DEFAULT NULL,
  `jumlah_hari` varchar(50) DEFAULT NULL,
  `uang_muka` double DEFAULT NULL,
  `over_time` double DEFAULT NULL,
  `fid_kendaraan` int(10) NOT NULL,
  `fid_user` int(10) NOT NULL,
  `harga_sewa` double NOT NULL,
  `status_sopir` int(10) NOT NULL,
  `status_order` int(10) NOT NULL,
  `fid_perusahaan` int(10) NOT NULL,
  `jenis_jaminan` varchar(77) NOT NULL,
  `info_jaminan` varchar(277) NOT NULL,
  `lon` varchar(255) DEFAULT NULL,
  `lat` varchar(255) DEFAULT NULL,
  `tujuan` varchar(255) NOT NULL,
  `bukti_transfer` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `data_order`
--

INSERT INTO `data_order` (`id_order`, `km_awal`, `km_akhir`, `tanggal_berangkat`, `jam_berangkat`, `tanggal_datang`, `jam_datang`, `jumlah_hari`, `uang_muka`, `over_time`, `fid_kendaraan`, `fid_user`, `harga_sewa`, `status_sopir`, `status_order`, `fid_perusahaan`, `jenis_jaminan`, `info_jaminan`, `lon`, `lat`, `tujuan`, `bukti_transfer`) VALUES
(1, 2000, 3500, '2017-07-11', '01:01:00', '2017-07-13', '01:58:00', '2', 300000, NULL, 1, 1, 400000, 1, 3, 1, '', '', NULL, NULL, '', ''),
(2, 50000, 60000, '2017-07-10', '11:01:00', '2017-07-13', '00:00:00', '3', 100000, NULL, 2, 0, 400000, 1, 3, 1, '', '', NULL, NULL, '', ''),
(5, 0, 0, '2017-07-12', '01:01:00', '2017-07-15', '00:00:00', NULL, 100000, NULL, 1, 2, 90000, 1, 3, 1, '', '', NULL, NULL, '', ''),
(6, NULL, NULL, '2017-07-14', NULL, '2017-07-15', NULL, '1', NULL, NULL, 1, 1, 0, 1, 1, 1, '', '', NULL, NULL, '', ''),
(7, NULL, NULL, '2017-07-13', NULL, '2017-07-14', NULL, '1', NULL, NULL, 1, 7, 0, 1, 2, 1, '', '', NULL, NULL, '', ''),
(8, 50000, 80000, '2017-07-21', '01:01:00', '2017-07-22', '01:01:00', '1', NULL, NULL, 5, 4, 1000000, 1, 3, 1, '', '', NULL, NULL, '', ''),
(9, NULL, NULL, '2017-07-14', NULL, '2017-07-15', NULL, '1', NULL, NULL, 1, 7, 0, 1, 3, 1, '', '', NULL, NULL, '', ''),
(10, NULL, NULL, '2017-07-14', NULL, '2017-07-15', NULL, '1', NULL, NULL, 2, 7, 0, 1, 2, 1, '', '', NULL, NULL, '', ''),
(11, NULL, NULL, '2017-07-14', NULL, '2017-07-15', NULL, '1', NULL, NULL, 2, 7, 0, 1, 2, 1, '', '', NULL, NULL, '', ''),
(12, NULL, NULL, '2017-07-14', NULL, '2017-07-25', NULL, '11', NULL, NULL, 11, 7, 0, 1, 3, 1, '', '', NULL, NULL, '', ''),
(13, 20000, 0, '2017-07-18', '02:00:00', '2017-07-20', '00:00:00', '2', NULL, NULL, 5, 7, 750000, 1, 2, 1, '', '', NULL, NULL, '', 'images.jpg'),
(14, NULL, NULL, '2017-07-17', NULL, '2017-07-18', NULL, '1', NULL, NULL, 9, 7, 0, 1, 1, 1, '', '', NULL, NULL, 'Jember', ''),
(15, NULL, NULL, '2017-07-18', NULL, '2017-07-19', NULL, '1', NULL, NULL, 15, 13, 0, 2, 1, 1, 'Deposit', '', NULL, NULL, 'Probolinggo', ''),
(16, NULL, NULL, '2017-07-18', NULL, '2017-07-19', NULL, '1', NULL, NULL, 9, 13, 0, 1, 1, 1, '', '', '', '', 'Madura', ''),
(17, NULL, NULL, '2017-07-18', NULL, '2017-07-19', NULL, '1', NULL, NULL, 6, 13, 0, 1, 1, 1, '', '', NULL, NULL, 'Madura', ''),
(18, NULL, NULL, '2017-07-18', NULL, '2017-07-19', NULL, '1', NULL, NULL, 10, 13, 0, 2, 1, 1, 'Deposit', '', '112.719648', '-7.504118', 'Kesuksesan', '');

-- --------------------------------------------------------

--
-- Table structure for table `harga_sewa`
--

CREATE TABLE `harga_sewa` (
  `id_harga_sewa` int(10) NOT NULL,
  `fid_merk` int(10) NOT NULL,
  `harga_sewa12` double DEFAULT NULL,
  `harga_sewa24` double DEFAULT NULL,
  `fid_perusahaan` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `harga_sewa`
--

INSERT INTO `harga_sewa` (`id_harga_sewa`, `fid_merk`, `harga_sewa12`, `harga_sewa24`, `fid_perusahaan`) VALUES
(6, 1, 225000, 275000, 1),
(7, 2, 225000, 275000, 1),
(8, 3, 225000, 275000, 1),
(9, 4, 225000, 275000, 1),
(10, 5, 225000, 275000, 1),
(11, 8, 225000, 275000, 1),
(12, 9, 225000, 275000, 1),
(13, 10, 225000, 275000, 1),
(14, 11, 225000, 275000, 1),
(15, 12, 225000, 275000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `kendaraan`
--

CREATE TABLE `kendaraan` (
  `id_kendaraan` int(10) NOT NULL,
  `nopol_kendaraan` varchar(50) NOT NULL,
  `nama_pemilik` varchar(50) NOT NULL,
  `alamat_pemilik` text,
  `fid_merk` int(10) NOT NULL,
  `norangka_kendaraan` varchar(50) NOT NULL,
  `nomesin_kendaraan` varchar(50) NOT NULL,
  `tahun_kendaraan` varchar(50) NOT NULL,
  `warna_kendaraan` varchar(50) DEFAULT NULL,
  `nobpkb_kendaraan` varchar(50) NOT NULL,
  `tanggal_invest` date DEFAULT NULL,
  `masa_stnk` date DEFAULT NULL,
  `masa_pajak` date DEFAULT NULL,
  `isi_silinder` varchar(50) DEFAULT NULL,
  `fid_perusahaan` int(10) NOT NULL,
  `status_kendaraan` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kendaraan`
--

INSERT INTO `kendaraan` (`id_kendaraan`, `nopol_kendaraan`, `nama_pemilik`, `alamat_pemilik`, `fid_merk`, `norangka_kendaraan`, `nomesin_kendaraan`, `tahun_kendaraan`, `warna_kendaraan`, `nobpkb_kendaraan`, `tanggal_invest`, `masa_stnk`, `masa_pajak`, `isi_silinder`, `fid_perusahaan`, `status_kendaraan`) VALUES
(1, 'L 1103 YM', 'Kastining', 'Alun-alun Bangunsari Utara 24B RW05/RW07 Kel. Dupak Kec. Krembangan SBY', 1, 'MHFXS42G2D2550826', '2KDS281052', '2013', 'Silver Metalik', '-', NULL, '2021-03-24', '2021-03-23', '02494CC', 1, 2),
(2, 'L 1633 HJ', 'Mohammad Socheh', 'Sidosermo 1/14 RW01/RT/02 Kel Sidosermo Kec. Wonocolo SBY', 1, 'MHFXS42G2B2527298', '2KD6711200', '2011', 'Hitam Metalik', 'H08420551', NULL, '2021-02-17', '2018-02-17', '02494CC', 1, 2),
(3, 'L 1779 MI', 'Eko Purnomo', 'A. Yani NO.68-70 RW08/RT01 Kel. Ketintang Kec. Gayungan SBY', 2, 'MHKM5EA3JGJ024665', 'INFR081048', '2016', 'Hitam Metalik', 'MO4613598', NULL, '2021-04-09', '2017-04-09', '01329CC', 1, 1),
(4, 'L 7729 G', 'Kanigara Jaya Raya. PT', 'Sidosermo Indah Raya No. 5 RT00/RW00 Kel. Sidosermo Kec, Wonocolo SBY', 8, 'JTFSS22P6EO135183', '2KDA55340', '2014', 'Silver Metalik', 'L05191279', NULL, '2019-10-23', '2018-02-17', '02494CC', 1, 1),
(5, 'L 7746 G', 'Kanigara Jaya Raya. PT', 'Sidosermo Indah Raya No. 5 RT00/RW00 Kel. Sidosermo Kec, Wonocolo SBY', 8, 'JTFSS22P0EO137883', '2KDA633300', '2014', 'Silver Metalik', 'L07609776', NULL, '2019-12-11', '2016-12-11', '02494CC', 1, 2),
(6, 'L 1683 HO', 'Diah Sri Rahayu', 'Bendul Merisi Selatan 5/34 RW10/RT04 Kel. Bendul Merisi Kec. Wonocolo SBY', 4, 'MHFJB8EM1G1012773', '2GD4227373', '2016', 'Hitam Metalik', 'M10003890', NULL, '2022-01-27', '2018-01-27', '02393CC', 1, 1),
(7, 'N 7223 A', 'Ranti Hanyani', 'Perum River Side A4-3 RW05/RT01 Kel. Balearjosari Kec. Blimbing MLG', 8, 'JTFSS22P0E0139763', '2KDA630239', '2014', 'Silver Metalik', '-', NULL, '2020-01-19', NULL, '02494CC', 1, 1),
(8, 'L 9605 B', 'Esan Wiyadi', 'Juwingan 1/32 RT11/RW/10 Kel. Baratajaya Kec. Gubeng SBY', 10, 'MHYESL415BJ218504', 'G15AID833456', '2011', 'Hitam', 'I06433383', NULL, '2017-01-11', '2016-01-11', '01493CC', 1, 1),
(9, 'L 9390 M', 'Kanigara Jaya Raya. PT', 'Sidosermo Indah Raya No. 5 RT00/RW00 Kel. Sidosermo Kec, Wonocolo SBY', 10, 'MHYESL415EJ304535', 'G15AID940864', '2014', 'Hitam', 'K06708659', NULL, '2019-03-10', '2017-03-10', '01493CC', 1, 1),
(10, 'L 9280 GF', 'Isdiyah', 'Ubi 2/5 RW05/RT 02 Kel Jagir Kec. Wonokromo SBY', 11, 'MHKP3CA1JFK104094', '3SZDFS6772', '2015', 'Putih', 'MO2367321', NULL, '2020-11-18', '2017-11-18', '01493CC', 1, 1),
(11, 'W 1633 RZ', 'Dwi Noto Harto', 'Tebel Barat RW01 RT02 Ds. Tebel Kec. Gedangan Sda', 5, 'MNKV5EA2JFJ001943', '1NRF019706', '2015', 'Putih', 'M03138679', NULL, '2020-12-23', '2017-12-23', '1329CC', 1, 5),
(12, 'M 1326 HC', 'Eko Widjiono', 'Dsn Petengan RW04 RT04 Ds. Tengket Kec. Arosbaya BKL', 12, 'MHKS6GJ6JGJ004425', '3NRH025264', '2016', 'Putih', 'M08809527', NULL, '2021-10-17', '2017-10-17', '1197CC', 1, 1),
(13, 'L 1488 MO', 'Sjarifah A. Sarin', 'Sidosermo Indah No. 10-18 RW06 RT03 Kel Sidosermo Kec. Wonocolo SBY', 2, 'MHKM5EA2JGK013326', '1NRF184838', '2016', 'Silver Metalik', 'M09091201', NULL, '2021-11-01', '2021-11-01', '1329CC', 1, 1),
(14, 'L1586MT', 'Kanigara Jaya Raya, PT', 'Sidosermo Indah Raya No. 5 RW00 RT00 Kel Sidosermo Kec. Wonocolo SBY', 2, 'MHKM5EA2JGK008520', '1NRF156908', '2016', 'Silver Metalik', 'M08651194', NULL, '2021-08-23', '2017-08-23', '1329CC', 1, 1),
(15, 'F 7085 FC', 'Kurnia Tjahja Wicaksono', 'Legenda wisata blok K2 No. 5 RT03/RW19 G -N Putri Kai Bogor', 8, 'JTFSS22P5DO126361', '2KDA128977', '2013', 'Silver Metalik', '-', NULL, '2018-11-07', '2017-11-07', '02494CC', 1, 1),
(16, 'W 881 YD', 'Sri Aryanawati', 'Kalijateng Gang II/7 RW01/RT05 Kel Kalijaten Kec. Taman Sidoarjo', 3, 'MHFXS41G1F1518184', '2KDS530545', '2015', 'Hitam Metalik', '-', NULL, '2020-06-11', '2018-06-11', '02494CC', 1, 1),
(17, 'W 1799 SC', 'Moh. Toni', 'H. Syukur II/8 A RW 08/RT16 Ds. Sedati Gede Kec. Sedati SDA', 5, 'MNKV5EA2JGJ006858', '1NRF135250', '2016', 'Putih', 'M06455613', NULL, '2021-07-01', '2017-07-01', '1329CC', 1, 1),
(18, 'L 1907 DH', 'RR. Rahayu Ernawati, Drh', 'Sutorejo Tengah 12/26 RW 08/RT 11 Kel. DK Sutorejo Kec. Mulyorejo Surabaya', 13, 'MHKV1BA1JCK007741', 'DL38199', '2012', 'Biru Tua Metalik', 'I11794723', NULL, '2017-07-17', '2017-07-17', '01298CC', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `merk_kendaraan`
--

CREATE TABLE `merk_kendaraan` (
  `id_merk` int(10) NOT NULL,
  `merk_kendaraan` varchar(50) NOT NULL,
  `produsen_kendaraan` varchar(50) NOT NULL,
  `fid_perusahaan` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `merk_kendaraan`
--

INSERT INTO `merk_kendaraan` (`id_merk`, `merk_kendaraan`, `produsen_kendaraan`, `fid_perusahaan`) VALUES
(1, 'Kijang Innova G XS42 DS\r\n', 'TOYOTA', 1),
(2, 'Avanza 1.3 G F653 RM MT\r\n', 'TOYOTA', 1),
(3, 'Kijang Innova E XS41 DS\r\n', 'TOYOTA', 1),
(4, 'Kijang Innova 2.4 G MT\r\n', 'TOYOTA', 1),
(5, 'Xenia 1.3 R MT\r\n', 'DAIHATSU', 1),
(8, 'Hi Ace Commuter MT\r\n', 'TOYOTA', 1),
(9, 'Xenia 1.3 R MT F653RVGM\r\n', 'DAIHATSU', 1),
(10, 'ST150\r\n', 'FORD', 1),
(11, 'S402RPPMRFJJ KJ\r\n', 'DAIHATSU', 1),
(12, 'B401RS GMZFJ 1.2R MT (SIGRA)\r\n', 'DAIHATSU', 1),
(13, 'F651RV GMRFJ 4X2 MT\r\n', 'DAIHATSU', 1);

-- --------------------------------------------------------

--
-- Table structure for table `misi`
--

CREATE TABLE `misi` (
  `id_misi` int(10) NOT NULL,
  `misi` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `misi`
--

INSERT INTO `misi` (`id_misi`, `misi`) VALUES
(1, 'Menjadi perusahaan terdepan di bidang pelayanan jasa maupun pengadaan barang.'),
(2, 'Menjadi leading sector di area Surabaya,Jawa Timur hingga Nasional.'),
(3, 'Menjadi Perusahaan Nomer 1 dalam Pelayanan Prima terhadap Konsumen');

-- --------------------------------------------------------

--
-- Table structure for table `partner`
--

CREATE TABLE `partner` (
  `id_partner` int(10) NOT NULL,
  `nama_partner` text NOT NULL,
  `deskripsi_partner` text,
  `file` varchar(100) NOT NULL,
  `type` varchar(10) NOT NULL,
  `size` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `partner`
--

INSERT INTO `partner` (`id_partner`, `nama_partner`, `deskripsi_partner`, `file`, `type`, `size`) VALUES
(1, 'Grab', 'Grab adalah salah satu provider taksi dan ojek Online di Indonesia', '73365-grab.png', 'image/png', 17873),
(2, 'Gojek', 'Gojek adalah salah satu provider taksi dan ojek Online di Indonesia', '80305-gojek.jpg', 'image/jpeg', 29385),
(3, 'Uber', 'Uber adalah salah satu provider taksi dan ojek Online di Indonesia', '18444-uber-serp-logo-f6e7549c89.jpg', 'image/jpeg', 16190);

-- --------------------------------------------------------

--
-- Table structure for table `perusahaan`
--

CREATE TABLE `perusahaan` (
  `id_perusahaan` int(10) NOT NULL,
  `perusahaan` varchar(100) DEFAULT NULL,
  `alamat_perusahaan` text,
  `telepon_perusahaan` varchar(50) DEFAULT NULL,
  `pimpinan_perusahaan` varchar(255) DEFAULT NULL,
  `foto_logo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `perusahaan`
--

INSERT INTO `perusahaan` (`id_perusahaan`, `perusahaan`, `alamat_perusahaan`, `telepon_perusahaan`, `pimpinan_perusahaan`, `foto_logo`) VALUES
(1, 'KANIGARA TRANS', 'Sidosermo Indah No. 5 Surabaya', '(031) 8410 214', 'Mochtar Helmi', 'logo.png'),
(2, 'JJ TRANS', 'Bendul Merisi', '0318823929', 'Rendi', NULL),
(3, 'PT BIMASAKTI TRI JAYA', 'Margorejo Indah', '0312323', NULL, NULL),
(4, 'BBS TRANS', 'Mulyosari', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_daftarhp`
--

CREATE TABLE `tb_daftarhp` (
  `id` int(7) NOT NULL,
  `nohp` varchar(27) NOT NULL,
  `verification_code` varchar(4) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_daftarhp`
--

INSERT INTO `tb_daftarhp` (`id`, `nohp`, `verification_code`, `status`) VALUES
(1, '081217123687', '8253', 2),
(4, '08178787878', '6852', 1),
(7, '089567878889', '6315', 2);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(10) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `akses` int(4) NOT NULL,
  `fid_perusahaan` int(10) NOT NULL,
  `fid_user` int(10) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `username`, `password`, `akses`, `fid_perusahaan`, `fid_user`, `status`) VALUES
(1, 'admin', 'admin', 1, 0, 0, 0),
(2, 'kanigara', 'kanigara', 2, 1, 0, 0),
(3, 'riyanto', 'riyanto', 3, 0, 1, 2),
(4, 'romosianturi', 'romosianturi', 3, 0, 2, 0),
(5, 'joniiskandar', 'joniiskandar', 3, 0, 4, 2),
(6, 'syaiful', 'syaiful', 3, 0, 7, 2),
(9, 'sultan', 'sultan', 3, 0, 10, 1),
(12, 'cikyun', 'cikyun', 3, 0, 13, 2);

-- --------------------------------------------------------

--
-- Table structure for table `visi`
--

CREATE TABLE `visi` (
  `id_visi` int(10) NOT NULL,
  `visi` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `visi`
--

INSERT INTO `visi` (`id_visi`, `visi`) VALUES
(1, 'Visi dari LIMO INDO adalah menjadi perusahaan dibidang jasa transportasi nomor satu di Indonesia. Baik itu dalam kecanggihan teknologi maupun kualitas pelayanan jasa yang diberikan. Visi tersebut didukung sepenuhnya dengan beberapa misi penunjang di bawah ini.');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `data_maintenance`
--
ALTER TABLE `data_maintenance`
  ADD PRIMARY KEY (`id_maintenance`);

--
-- Indexes for table `data_order`
--
ALTER TABLE `data_order`
  ADD PRIMARY KEY (`id_order`);

--
-- Indexes for table `harga_sewa`
--
ALTER TABLE `harga_sewa`
  ADD PRIMARY KEY (`id_harga_sewa`);

--
-- Indexes for table `kendaraan`
--
ALTER TABLE `kendaraan`
  ADD PRIMARY KEY (`id_kendaraan`);

--
-- Indexes for table `merk_kendaraan`
--
ALTER TABLE `merk_kendaraan`
  ADD PRIMARY KEY (`id_merk`);

--
-- Indexes for table `misi`
--
ALTER TABLE `misi`
  ADD PRIMARY KEY (`id_misi`);

--
-- Indexes for table `partner`
--
ALTER TABLE `partner`
  ADD PRIMARY KEY (`id_partner`);

--
-- Indexes for table `perusahaan`
--
ALTER TABLE `perusahaan`
  ADD PRIMARY KEY (`id_perusahaan`);

--
-- Indexes for table `tb_daftarhp`
--
ALTER TABLE `tb_daftarhp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `visi`
--
ALTER TABLE `visi`
  ADD PRIMARY KEY (`id_visi`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
  MODIFY `id_user` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `data_maintenance`
--
ALTER TABLE `data_maintenance`
  MODIFY `id_maintenance` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `data_order`
--
ALTER TABLE `data_order`
  MODIFY `id_order` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `harga_sewa`
--
ALTER TABLE `harga_sewa`
  MODIFY `id_harga_sewa` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `kendaraan`
--
ALTER TABLE `kendaraan`
  MODIFY `id_kendaraan` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `merk_kendaraan`
--
ALTER TABLE `merk_kendaraan`
  MODIFY `id_merk` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `misi`
--
ALTER TABLE `misi`
  MODIFY `id_misi` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `partner`
--
ALTER TABLE `partner`
  MODIFY `id_partner` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `perusahaan`
--
ALTER TABLE `perusahaan`
  MODIFY `id_perusahaan` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_daftarhp`
--
ALTER TABLE `tb_daftarhp`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `visi`
--
ALTER TABLE `visi`
  MODIFY `id_visi` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
