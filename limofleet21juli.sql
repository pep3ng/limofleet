/*
Navicat MySQL Data Transfer

Source Server         : lokal
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : kanigaratrans_db

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2017-07-21 15:54:49
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `client`
-- ----------------------------
DROP TABLE IF EXISTS `client`;
CREATE TABLE `client` (
  `id_user` int(10) NOT NULL AUTO_INCREMENT,
  `nama_user` varchar(50) DEFAULT NULL,
  `alamat_user` text,
  `telepon_user` text,
  `email_user` text,
  `foto_diri` varchar(255) DEFAULT NULL,
  `foto_ktp` varchar(255) DEFAULT NULL,
  `foto_sima` varchar(255) DEFAULT NULL,
  `foto_ksk` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of client
-- ----------------------------
INSERT INTO `client` VALUES ('1', 'Riyanto', 'Bendul Merisi Selatan IV/72', '082382381', 'riyanto@gmail.com', 'avatar.jpg', 'ktp.jpg', 'sim.jpg', 'ksk.jpg');
INSERT INTO `client` VALUES ('2', 'Romo Sianturi', 'Jalan Jenggolo 3 Sidoarjo', '023230230', 'romosianturi@gmail.com', '76178-foto.jpg', '23381-fotoktp.jpg', '27614-sima.jpg', '66688-fotoksk.jpg');
INSERT INTO `client` VALUES ('4', 'Joni', 'Buduran 5 Sidoarjo', '08123123', 'joni@gmail.com', '30740-flyer A52.jpg', '53756-flyer A5.jpg', '28289-flyer a5 3.jpg', '49047-flyer a5 4.jpg');
INSERT INTO `client` VALUES ('7', 'Bp. Syaiful Alamsah', 'Kalitengah Utara No.37', '081217123687', 'syaiful.sr@gmail.com', 'Kimono-laki1.jpg', 'Kimono-laki1.jpg', 'Kimono-laki1.jpg', 'Kimono-laki1.jpg');
INSERT INTO `client` VALUES ('10', 'Bp. Sultan Rafly', 'kalitengah utara no.37', '08178787878', 'sultan@gmail.com', 'Kimono-laki1.jpg', 'Kimono-laki1.jpg', 'Kimono-laki1.jpg', 'Kimono-laki1.jpg');
INSERT INTO `client` VALUES ('13', 'Ibu. cikyun nyimas', 'kalitengah utara no.37', '089567878889', 'cikyun8@gmail.com', 'Kimono-laki1.jpg', 'images.jpg', 'images.jpg', 'images.jpg');

-- ----------------------------
-- Table structure for `data_maintenance`
-- ----------------------------
DROP TABLE IF EXISTS `data_maintenance`;
CREATE TABLE `data_maintenance` (
  `id_maintenance` int(10) NOT NULL AUTO_INCREMENT,
  `tanggal_masuk` date DEFAULT NULL,
  `tanggal_keluar` date DEFAULT NULL,
  `fid_kendaraan` int(10) NOT NULL,
  `fid_user` int(10) NOT NULL,
  `status_maintenance` int(10) NOT NULL,
  `estimasi_biaya` double DEFAULT NULL,
  `detail_service` text,
  `detail_cleaning` text,
  `fid_perusahaan` int(10) NOT NULL,
  PRIMARY KEY (`id_maintenance`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of data_maintenance
-- ----------------------------

-- ----------------------------
-- Table structure for `data_order`
-- ----------------------------
DROP TABLE IF EXISTS `data_order`;
CREATE TABLE `data_order` (
  `id_order` int(10) NOT NULL AUTO_INCREMENT,
  `km_awal` double DEFAULT NULL,
  `km_akhir` double DEFAULT NULL,
  `tanggal_berangkat` varchar(8) DEFAULT NULL,
  `jam_berangkat` time DEFAULT NULL,
  `tanggal_datang` varchar(8) DEFAULT NULL,
  `jam_datang` time DEFAULT NULL,
  `jumlah_hari` varchar(50) DEFAULT NULL,
  `uang_muka` double DEFAULT NULL,
  `over_time` double DEFAULT NULL,
  `fid_merk` int(10) NOT NULL,
  `fid_kendaraan` int(10) NOT NULL,
  `fid_user` int(10) NOT NULL,
  `harga_sewa` double NOT NULL,
  `status_sopir` int(10) NOT NULL,
  `status_order` int(10) NOT NULL,
  `fid_perusahaan` int(10) NOT NULL,
  `jenis_jaminan` varchar(77) NOT NULL,
  `info_jaminan` varchar(277) NOT NULL,
  `lon` varchar(255) DEFAULT NULL,
  `lat` varchar(255) DEFAULT NULL,
  `tujuan` varchar(255) NOT NULL,
  `bukti_transfer` varchar(255) NOT NULL,
  PRIMARY KEY (`id_order`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of data_order
-- ----------------------------
INSERT INTO `data_order` VALUES ('1', '9000', '12000', '20170721', '01:01:00', '20170722', '00:00:00', null, '900000', null, '3', '18', '2', '400000', '1', '2', '1', '', '', '106.8593333', '-6.201889', 'Bekasi', '');

-- ----------------------------
-- Table structure for `harga_sewa`
-- ----------------------------
DROP TABLE IF EXISTS `harga_sewa`;
CREATE TABLE `harga_sewa` (
  `id_harga_sewa` int(10) NOT NULL AUTO_INCREMENT,
  `fid_merk` int(10) NOT NULL,
  `harga_sewa12` double DEFAULT NULL,
  `harga_sewa24` double DEFAULT NULL,
  `fid_perusahaan` int(10) NOT NULL,
  PRIMARY KEY (`id_harga_sewa`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of harga_sewa
-- ----------------------------
INSERT INTO `harga_sewa` VALUES ('6', '1', '225000', '275000', '1');
INSERT INTO `harga_sewa` VALUES ('7', '2', '225000', '275000', '1');
INSERT INTO `harga_sewa` VALUES ('8', '3', '225000', '275000', '1');
INSERT INTO `harga_sewa` VALUES ('9', '4', '225000', '275000', '1');
INSERT INTO `harga_sewa` VALUES ('10', '5', '225000', '275000', '1');
INSERT INTO `harga_sewa` VALUES ('11', '8', '225000', '275000', '1');
INSERT INTO `harga_sewa` VALUES ('12', '9', '225000', '275000', '1');
INSERT INTO `harga_sewa` VALUES ('13', '10', '225000', '275000', '1');
INSERT INTO `harga_sewa` VALUES ('14', '11', '225000', '275000', '1');
INSERT INTO `harga_sewa` VALUES ('15', '12', '225000', '275000', '1');

-- ----------------------------
-- Table structure for `kendaraan`
-- ----------------------------
DROP TABLE IF EXISTS `kendaraan`;
CREATE TABLE `kendaraan` (
  `id_kendaraan` int(10) NOT NULL AUTO_INCREMENT,
  `nopol_kendaraan` varchar(50) NOT NULL,
  `nama_pemilik` varchar(50) NOT NULL,
  `alamat_pemilik` text,
  `fid_merk` int(10) NOT NULL,
  `norangka_kendaraan` varchar(50) NOT NULL,
  `nomesin_kendaraan` varchar(50) NOT NULL,
  `tahun_kendaraan` varchar(50) NOT NULL,
  `warna_kendaraan` varchar(50) DEFAULT NULL,
  `nobpkb_kendaraan` varchar(50) NOT NULL,
  `tanggal_invest` date DEFAULT NULL,
  `masa_stnk` date DEFAULT NULL,
  `masa_pajak` date DEFAULT NULL,
  `isi_silinder` varchar(50) DEFAULT NULL,
  `fid_perusahaan` int(10) NOT NULL,
  `status_kendaraan` int(10) DEFAULT NULL,
  `lon` varchar(255) DEFAULT NULL,
  `lat` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_kendaraan`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of kendaraan
-- ----------------------------
INSERT INTO `kendaraan` VALUES ('1', 'L 1103 YM', 'Kastining', 'Alun-alun Bangunsari Utara 24B RW05/RW07 Kel. Dupak Kec. Krembangan SBY', '1', 'MHFXS42G2D2550826', '2KDS281052', '2013', 'Silver Metalik', '-', null, '2021-03-24', '2021-03-23', '02494CC', '1', '1', null, null);
INSERT INTO `kendaraan` VALUES ('2', 'L 1633 HJ', 'Mohammad Socheh', 'Sidosermo 1/14 RW01/RT/02 Kel Sidosermo Kec. Wonocolo SBY', '1', 'MHFXS42G2B2527298', '2KD6711200', '2011', 'Hitam Metalik', 'H08420551', null, '2021-02-17', '2018-02-17', '02494CC', '1', '1', null, null);
INSERT INTO `kendaraan` VALUES ('3', 'L 1779 MI', 'Eko Purnomo', 'A. Yani NO.68-70 RW08/RT01 Kel. Ketintang Kec. Gayungan SBY', '2', 'MHKM5EA3JGJ024665', 'INFR081048', '2016', 'Hitam Metalik', 'MO4613598', null, '2021-04-09', '2017-04-09', '01329CC', '1', '1', null, null);
INSERT INTO `kendaraan` VALUES ('4', 'L 7729 G', 'Kanigara Jaya Raya. PT', 'Sidosermo Indah Raya No. 5 RT00/RW00 Kel. Sidosermo Kec, Wonocolo SBY', '8', 'JTFSS22P6EO135183', '2KDA55340', '2014', 'Silver Metalik', 'L05191279', null, '2019-10-23', '2018-02-17', '02494CC', '1', '1', null, null);
INSERT INTO `kendaraan` VALUES ('5', 'L 7746 G', 'Kanigara Jaya Raya. PT', 'Sidosermo Indah Raya No. 5 RT00/RW00 Kel. Sidosermo Kec, Wonocolo SBY', '8', 'JTFSS22P0EO137883', '2KDA633300', '2014', 'Silver Metalik', 'L07609776', null, '2019-12-11', '2016-12-11', '02494CC', '1', '1', null, null);
INSERT INTO `kendaraan` VALUES ('6', 'L 1683 HO', 'Diah Sri Rahayu', 'Bendul Merisi Selatan 5/34 RW10/RT04 Kel. Bendul Merisi Kec. Wonocolo SBY', '4', 'MHFJB8EM1G1012773', '2GD4227373', '2016', 'Hitam Metalik', 'M10003890', null, '2022-01-27', '2018-01-27', '02393CC', '1', '1', null, null);
INSERT INTO `kendaraan` VALUES ('7', 'N 7223 A', 'Ranti Hanyani', 'Perum River Side A4-3 RW05/RT01 Kel. Balearjosari Kec. Blimbing MLG', '8', 'JTFSS22P0E0139763', '2KDA630239', '2014', 'Silver Metalik', '-', null, '2020-01-19', null, '02494CC', '1', '1', null, null);
INSERT INTO `kendaraan` VALUES ('8', 'L 9605 B', 'Esan Wiyadi', 'Juwingan 1/32 RT11/RW/10 Kel. Baratajaya Kec. Gubeng SBY', '10', 'MHYESL415BJ218504', 'G15AID833456', '2011', 'Hitam', 'I06433383', null, '2017-01-11', '2016-01-11', '01493CC', '1', '1', null, null);
INSERT INTO `kendaraan` VALUES ('9', 'L 9390 M', 'Kanigara Jaya Raya. PT', 'Sidosermo Indah Raya No. 5 RT00/RW00 Kel. Sidosermo Kec, Wonocolo SBY', '10', 'MHYESL415EJ304535', 'G15AID940864', '2014', 'Hitam', 'K06708659', null, '2019-03-10', '2017-03-10', '01493CC', '1', '1', null, null);
INSERT INTO `kendaraan` VALUES ('10', 'L 9280 GF', 'Isdiyah', 'Ubi 2/5 RW05/RT 02 Kel Jagir Kec. Wonokromo SBY', '11', 'MHKP3CA1JFK104094', '3SZDFS6772', '2015', 'Putih', 'MO2367321', null, '2020-11-18', '2017-11-18', '01493CC', '1', '1', null, null);
INSERT INTO `kendaraan` VALUES ('11', 'W 1633 RZ', 'Dwi Noto Harto', 'Tebel Barat RW01 RT02 Ds. Tebel Kec. Gedangan Sda', '5', 'MNKV5EA2JFJ001943', '1NRF019706', '2015', 'Putih', 'M03138679', null, '2020-12-23', '2017-12-23', '1329CC', '1', '1', null, null);
INSERT INTO `kendaraan` VALUES ('12', 'M 1326 HC', 'Eko Widjiono', 'Dsn Petengan RW04 RT04 Ds. Tengket Kec. Arosbaya BKL', '12', 'MHKS6GJ6JGJ004425', '3NRH025264', '2016', 'Putih', 'M08809527', null, '2021-10-17', '2017-10-17', '1197CC', '1', '1', null, null);
INSERT INTO `kendaraan` VALUES ('13', 'L 1488 MO', 'Sjarifah A. Sarin', 'Sidosermo Indah No. 10-18 RW06 RT03 Kel Sidosermo Kec. Wonocolo SBY', '2', 'MHKM5EA2JGK013326', '1NRF184838', '2016', 'Silver Metalik', 'M09091201', null, '2021-11-01', '2021-11-01', '1329CC', '1', '1', null, null);
INSERT INTO `kendaraan` VALUES ('14', 'L1586MT', 'Kanigara Jaya Raya, PT', 'Sidosermo Indah Raya No. 5 RW00 RT00 Kel Sidosermo Kec. Wonocolo SBY', '2', 'MHKM5EA2JGK008520', '1NRF156908', '2016', 'Silver Metalik', 'M08651194', null, '2021-08-23', '2017-08-23', '1329CC', '1', '1', null, null);
INSERT INTO `kendaraan` VALUES ('15', 'F 7085 FC', 'Kurnia Tjahja Wicaksono', 'Legenda wisata blok K2 No. 5 RT03/RW19 G -N Putri Kai Bogor', '8', 'JTFSS22P5DO126361', '2KDA128977', '2013', 'Silver Metalik', '-', null, '2018-11-07', '2017-11-07', '02494CC', '1', '1', null, null);
INSERT INTO `kendaraan` VALUES ('16', 'W 881 YD', 'Sri Aryanawati', 'Kalijateng Gang II/7 RW01/RT05 Kel Kalijaten Kec. Taman Sidoarjo', '3', 'MHFXS41G1F1518184', '2KDS530545', '2015', 'Hitam Metalik', '-', null, '2020-06-11', '2018-06-11', '02494CC', '1', '1', null, null);
INSERT INTO `kendaraan` VALUES ('17', 'W 1799 SC', 'Moh. Toni', 'H. Syukur II/8 A RW 08/RT16 Ds. Sedati Gede Kec. Sedati SDA', '5', 'MNKV5EA2JGJ006858', '1NRF135250', '2016', 'Putih', 'M06455613', null, '2021-07-01', '2017-07-01', '1329CC', '1', '1', null, null);
INSERT INTO `kendaraan` VALUES ('18', 'L 1907 DH', 'RR. Rahayu Ernawati, Drh', 'Sutorejo Tengah 12/26 RW 08/RT 11 Kel. DK Sutorejo Kec. Mulyorejo Surabaya', '13', 'MHKV1BA1JCK007741', 'DL38199', '2012', 'Biru Tua Metalik', 'I11794723', null, '2017-07-17', '2017-07-17', '01298CC', '1', '2', null, null);

-- ----------------------------
-- Table structure for `merk_kendaraan`
-- ----------------------------
DROP TABLE IF EXISTS `merk_kendaraan`;
CREATE TABLE `merk_kendaraan` (
  `id_merk` int(10) NOT NULL AUTO_INCREMENT,
  `merk_kendaraan` varchar(50) NOT NULL,
  `produsen_kendaraan` varchar(50) NOT NULL,
  `fid_perusahaan` int(10) DEFAULT NULL,
  PRIMARY KEY (`id_merk`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of merk_kendaraan
-- ----------------------------
INSERT INTO `merk_kendaraan` VALUES ('1', 'Kijang Innova G XS42 DS\r\n', 'TOYOTA', '1');
INSERT INTO `merk_kendaraan` VALUES ('2', 'Avanza 1.3 G F653 RM MT\r\n', 'TOYOTA', '1');
INSERT INTO `merk_kendaraan` VALUES ('3', 'Kijang Innova E XS41 DS\r\n', 'TOYOTA', '1');
INSERT INTO `merk_kendaraan` VALUES ('4', 'Kijang Innova 2.4 G MT\r\n', 'TOYOTA', '1');
INSERT INTO `merk_kendaraan` VALUES ('5', 'Xenia 1.3 R MT\r\n', 'DAIHATSU', '1');
INSERT INTO `merk_kendaraan` VALUES ('8', 'Hi Ace Commuter MT\r\n', 'TOYOTA', '1');
INSERT INTO `merk_kendaraan` VALUES ('9', 'Xenia 1.3 R MT F653RVGM\r\n', 'DAIHATSU', '1');
INSERT INTO `merk_kendaraan` VALUES ('10', 'ST150\r\n', 'FORD', '1');
INSERT INTO `merk_kendaraan` VALUES ('11', 'S402RPPMRFJJ KJ\r\n', 'DAIHATSU', '1');
INSERT INTO `merk_kendaraan` VALUES ('12', 'B401RS GMZFJ 1.2R MT (SIGRA)\r\n', 'DAIHATSU', '1');
INSERT INTO `merk_kendaraan` VALUES ('13', 'F651RV GMRFJ 4X2 MT\r\n', 'DAIHATSU', '1');

-- ----------------------------
-- Table structure for `misi`
-- ----------------------------
DROP TABLE IF EXISTS `misi`;
CREATE TABLE `misi` (
  `id_misi` int(10) NOT NULL AUTO_INCREMENT,
  `misi` text,
  PRIMARY KEY (`id_misi`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of misi
-- ----------------------------
INSERT INTO `misi` VALUES ('1', 'Menjadi perusahaan terdepan di bidang pelayanan jasa maupun pengadaan barang.');
INSERT INTO `misi` VALUES ('2', 'Menjadi leading sector di area Surabaya,Jawa Timur hingga Nasional.');
INSERT INTO `misi` VALUES ('3', 'Menjadi Perusahaan Nomer 1 dalam Pelayanan Prima terhadap Konsumen');

-- ----------------------------
-- Table structure for `partner`
-- ----------------------------
DROP TABLE IF EXISTS `partner`;
CREATE TABLE `partner` (
  `id_partner` int(10) NOT NULL AUTO_INCREMENT,
  `nama_partner` text NOT NULL,
  `deskripsi_partner` text,
  `file` varchar(100) NOT NULL,
  `type` varchar(10) NOT NULL,
  `size` int(11) NOT NULL,
  PRIMARY KEY (`id_partner`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of partner
-- ----------------------------
INSERT INTO `partner` VALUES ('1', 'Grab', 'Grab adalah salah satu provider taksi dan ojek Online di Indonesia', '73365-grab.png', 'image/png', '17873');
INSERT INTO `partner` VALUES ('2', 'Gojek', 'Gojek adalah salah satu provider taksi dan ojek Online di Indonesia', '80305-gojek.jpg', 'image/jpeg', '29385');
INSERT INTO `partner` VALUES ('3', 'Uber', 'Uber adalah salah satu provider taksi dan ojek Online di Indonesia', '18444-uber-serp-logo-f6e7549c89.jpg', 'image/jpeg', '16190');

-- ----------------------------
-- Table structure for `perusahaan`
-- ----------------------------
DROP TABLE IF EXISTS `perusahaan`;
CREATE TABLE `perusahaan` (
  `id_perusahaan` int(10) NOT NULL AUTO_INCREMENT,
  `perusahaan` varchar(100) DEFAULT NULL,
  `alamat_perusahaan` text,
  `telepon_perusahaan` varchar(50) DEFAULT NULL,
  `pimpinan_perusahaan` varchar(255) DEFAULT NULL,
  `foto_logo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_perusahaan`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of perusahaan
-- ----------------------------
INSERT INTO `perusahaan` VALUES ('1', 'KANIGARA TRANS', 'Sidosermo Indah No. 5 Surabaya', '(031) 8410 214', 'Mochtar Helmi', 'logo.png');
INSERT INTO `perusahaan` VALUES ('2', 'JJ TRANS', 'Bendul Merisi', '0318823929', 'Rendi', null);
INSERT INTO `perusahaan` VALUES ('3', 'PT BIMASAKTI TRI JAYA', 'Margorejo Indah', '0312323', null, null);
INSERT INTO `perusahaan` VALUES ('4', 'BBS TRANS', 'Mulyosari', null, null, null);

-- ----------------------------
-- Table structure for `tb_daftarhp`
-- ----------------------------
DROP TABLE IF EXISTS `tb_daftarhp`;
CREATE TABLE `tb_daftarhp` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `nohp` varchar(27) NOT NULL,
  `verification_code` varchar(4) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_daftarhp
-- ----------------------------
INSERT INTO `tb_daftarhp` VALUES ('1', '081217123687', '8253', '2');
INSERT INTO `tb_daftarhp` VALUES ('4', '08178787878', '6852', '1');
INSERT INTO `tb_daftarhp` VALUES ('7', '089567878889', '6315', '2');

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id_user` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `akses` int(4) NOT NULL,
  `fid_perusahaan` int(10) NOT NULL,
  `fid_user` int(10) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'admin', 'admin', '1', '0', '0', '0');
INSERT INTO `user` VALUES ('2', 'kanigara', 'kanigara', '2', '1', '0', '0');
INSERT INTO `user` VALUES ('3', 'riyanto', 'riyanto', '3', '0', '1', '2');
INSERT INTO `user` VALUES ('4', 'romosianturi', 'romosianturi', '3', '0', '2', '0');
INSERT INTO `user` VALUES ('5', 'joniiskandar', 'joniiskandar', '3', '0', '4', '2');
INSERT INTO `user` VALUES ('6', 'syaiful', 'syaiful', '3', '0', '7', '2');
INSERT INTO `user` VALUES ('9', 'sultan', 'sultan', '3', '0', '10', '1');
INSERT INTO `user` VALUES ('12', 'cikyun', 'cikyun', '3', '0', '13', '2');

-- ----------------------------
-- Table structure for `visi`
-- ----------------------------
DROP TABLE IF EXISTS `visi`;
CREATE TABLE `visi` (
  `id_visi` int(10) NOT NULL AUTO_INCREMENT,
  `visi` text,
  PRIMARY KEY (`id_visi`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of visi
-- ----------------------------
INSERT INTO `visi` VALUES ('1', 'Visi dari LIMO INDO adalah menjadi perusahaan dibidang jasa transportasi nomor satu di Indonesia. Baik itu dalam kecanggihan teknologi maupun kualitas pelayanan jasa yang diberikan. Visi tersebut didukung sepenuhnya dengan beberapa misi penunjang di bawah ini.');
