

<!-- Mirrored from admink.themerex.net/basic-profile.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 05 Jul 2017 05:08:44 GMT -->
<head>
    <!-- Meta and Title -->
    <meta charset="utf-8">
    <title>LIMO INDO - Partner Andalan Anda</title>
    <meta name="keywords" content="HTML5, Bootstrap 3, Admin Template, UI Theme"/>
    <meta name="description" content="AdminK - A Responsive HTML5 Admin UI Framework">
    <meta name="author" content="ThemeREX">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">

    <!-- Angular material -->
    <link rel="stylesheet" type="text/css" href="assets/skin/css/angular-material.min.css">
    
    <!-- Icomoon -->
    <link rel="stylesheet" type="text/css" href="assets/fonts/icomoon/icomoon.css">    
    
    <!-- AnimatedSVGIcons -->
    <link rel="stylesheet" type="text/css" href="assets/fonts/animatedsvgicons/css/codropsicons.css">

    <!-- CSS - allcp forms -->
    <link rel="stylesheet" type="text/css" href="assets/allcp/forms/css/forms.css">

    <!-- Plugins -->
    <link rel="stylesheet" type="text/css" href="assets/js/utility/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.min.css">

    <!-- CSS - theme -->
    <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/less/theme.css">
    
    <!-- IE8 HTML5 support -->
    <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body class="forms-elements" data-spy="scroll" data-target="#nav-spy" data-offset="300">


<!-- Body Wrap -->
<div id="main">

    <!-- Header  -->
    <header class="navbar navbar-fixed-top ">
        <ul class="nav navbar-nav navbar-right">

            <li class="dropdown dropdown-fuse">
                <div class="navbar-btn btn-group">
                    <button class="dropdown-toggle btn btn-hover-effects" data-toggle="dropdown">
                        <span class="fa fa-bell fs20 text-info-darker"></span>
                        <span class="fs14 visible-xl">
                            8
                        </span>
                    </button>
                    <div class="navbar-activity dropdown-menu keep-dropdown w375" role="menu">
                        <div class="panel mbn">
                            <div class="panel-menu">
                                <div class="btn-group btn-group-nav" role="tablist">
                                    <a href="#nav-tab4" data-toggle="tab"
                                       class="btn btn-primary btn-sm active">Activity</a>
                                </div>
                                <button class="btn btn-xs" type="button"><i
                                        class="fa fa-refresh"></i>
                                </button>
                            </div>
                            <div class="panel-body pn">
                                <div class="tab-content br-n pn">
                                    <div id="nav-tab4" class="tab-pane active" role="tabpanel">
                                        <ol class="timeline-list">
                                            <li class="timeline-item">
                                                <div class="timeline-icon light">
                                                    <span class="fa fa-envelope"></span>
                                                </div>
                                                <div class="timeline-desc">
                                                    <b>John Doe <span>-</span> <span class="timeline-date">3:16 am</span></b>
                                                    Sent you a message.
                                                    <a href="#">View now</a>
                                                </div>
                                            </li>
                                            <li class="timeline-item">
                                                <div class="timeline-icon">
                                                    <span class="fa fa-info-circle"></span>
                                                </div>
                                                <div class="timeline-desc">
                                                    <b>Admin <span>-</span> <span class="timeline-date">6:26 pm</span></b> 
                                                    Сreated invoice for:
                                                    <a href="#">iPad Air</a>
                                                </div>
                                            </li>
                                            <li class="timeline-item">
                                                <div class="timeline-icon">
                                                    <span class="fa fa-info-circle"></span>
                                                </div>
                                                <div class="timeline-desc">
                                                    <b>Admin <span>-</span> <span class="timeline-date">11:45 am</span></b> 
                                                    Сreated invoice for:
                                                    <a href="#">iPhone 5s</a>
                                                </div>
                                            </li>
                                        </ol>
                                    </div>
                                </div>

                            </div>
                            <div class="panel-footer text-center">
                                <a href="#" class="btn btn-warning btn-sm"> View All </a>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <li class="dropdown dropdown-fuse navbar-user">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <img class="btn-hover-effects" src="assets/img/avatars/profile_avatar.jpg" alt="avatar">
                    <span class="hidden-xs">
                        <span class="name"><?php echo $_SESSION['username']; ?></span>
                    </span>
                    <span class="fa fa-caret-down hidden-xs"></span>
                </a>
                <ul class="dropdown-menu list-group keep-dropdown w230" role="menu">
           
                    <li class="dropdown-footer text-center">
                        <a href="logout.php" class="btn btn-warning">
                            logout 
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </header>
    <!-- /Header -->

    <!-- Sidebar  -->
    <aside id="sidebar_left" class="nano affix">

        <!-- Sidebar Left Wrapper  -->
        <div class="sidebar-left-content nano-content">

            <!-- Sidebar Header -->
            <header class="sidebar-header">

                <!-- Sidebar - Logo -->
                <div class="sidebar-widget logo-widget">
                    <div class="media">
                        <a class="logo-image" href="index_user.php">
                            <img src="assets/img/logo.png" alt="" class="img-responsive">
                        </a>

                        <div class="logo-slogan">
                            <div>Fleet<span class="text-info">User</span></div>
                        </div>
                    </div>
                </div>

            </header>
            <!-- /Sidebar Header -->

            <!-- Sidebar Menu  -->
            <ul class="nav sidebar-menu">
                <li class="sidebar-label pt30">Navigation</li>
                <li>
                    <a href="index_user.php">
                        <span class="caret"></span>
                        <span class="sidebar-title">Profile</span>
                        <span class="sb-menu-icon fa fa-home"></span>
                    </a>
                </li>
					 <li>
                    <a  href="order_user.php">
                        <span class="caret"></span>
                        <span class="sidebar-title">Order</span>
                        <span class="sb-menu-icon fa fa-home"></span>
                    </a>
                </li>

            </ul>
            <!-- /Sidebar Menu  -->

        </div>
        <!-- /Sidebar Left Wrapper  -->

    </aside>
    <!-- /Sidebar -->