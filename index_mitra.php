<?php include "session_cek.php" ?>
<html>
<?php include "header_admin.php"; ?>

    <!-- Main Wrapper -->
    <section id="content_wrapper">

        <!-- Content -->
        <section id="content" class="animated fadeIn">

            <div class="row">
                <div class="col-md-12 mb40">

                    <div class="tab-block">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab1" data-toggle="tab">Profile Perusahaan</a>
                            </li>
                        </ul>
                        <div class="tab-content p30">
                            <div id="tab1" class="tab-pane active">

                    <?php
						$fid_perusahaan=$_SESSION['fid_perusahaan'];
						include ("db.php");
						$sql = mysqli_query($koneksi,"SELECT * FROM perusahaan where id_perusahaan='$fid_perusahaan'") or die("error sql");
						while($row = mysqli_fetch_assoc($sql)){
							$perusahaan=$row['perusahaan'];
							$alamat_perusahaan=$row['alamat_perusahaan'];
							$telepon_perusahaan=$row['telepon_perusahaan'];
							$pimpinan_perusahaan=$row['pimpinan_perusahaan'];
							$foto_logo=$row['foto_logo'];
						}
						?>
                    <div class="panel">
                        <div class="panel-heading">
                            <span class="panel-title">Profile</span>
                        </div>
                        <div class="panel-body">

                            <form class="form-horizontal">
							<div class="form-group">
                                    <label class="col-lg-3 control-label">Logo Perusahaan</label>

                                    <div class="col-lg-8">
                                        <div class="bs-component">
                                            <img class="mw140 mr25 mb20" src="assets/img/upload/<?php echo $foto_logo; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Nama</label>

                                    <div class="col-lg-8">
                                        <div class="bs-component">
                                            <p class="form-control-static text-muted"><?php echo $perusahaan; ?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label" for="textArea3">Alamat</label>

                                    <div class="col-lg-8">
                                        <div class="bs-component">
                                              <textarea class="form-control" id="textArea3" rows="3"
                                                        disabled><?php echo $alamat_perusahaan; ?></textarea>
                                        </div>
                                    </div>
                                </div>
								   <div class="form-group">
                                    <label class="col-lg-3 control-label">No.Telepon</label>

                                    <div class="col-lg-8">
                                        <div class="bs-component">
                                            <p class="form-control-static text-muted"><?php echo $telepon_perusahaan; ?></p>
                                        </div>
                                    </div>
                                </div>
								   <div class="form-group">
                                    <label class="col-lg-3 control-label">Alamat Email</label>

                                    <div class="col-lg-8">
                                        <div class="bs-component">
                                            <p class="form-control-static text-muted"><?php echo $pimpinan_perusahaan; ?></p>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">

                </div>
            </div>

        </section>
        <!-- /Content -->

    </section>


</div>
<!-- /Body Wrap  -->


<!-- Scripts -->

<!-- jQuery -->
<script src="assets/js/jquery/jquery-1.12.3.min.js"></script>
<script src="assets/js/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- AnimatedSVGIcons -->
<script src="assets/fonts/animatedsvgicons/js/snap.svg-min.js"></script>
<script src="assets/fonts/animatedsvgicons/js/svgicons-config.js"></script>
<script src="assets/fonts/animatedsvgicons/js/svgicons.js"></script>
<script src="assets/fonts/animatedsvgicons/js/svgicons-init.js"></script>

<!-- HighCharts Plugin -->
<script src="assets/js/plugins/highcharts/highcharts.js"></script>

<!-- Scroll -->
<script src="assets/js/utility/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.concat.min.js"></script>

<!-- Theme Scripts -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script src="assets/js/demo/widgets_sidebar.js"></script>
<script src="assets/js/pages/dashboard_init.js"></script>


<!-- /Scripts -->

</body>


<!-- Mirrored from admink.themerex.net/basic-profile.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 05 Jul 2017 05:08:47 GMT -->
</html>