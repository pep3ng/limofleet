
<html>
<?php include "header.php"; ?>

<body class="utility-page sb-l-c sb-r-c">

<!-- Body Wrap -->
<div id="main" class="animated fadeIn">

    <!-- Main Wrapper -->
    <section id="content_wrapper">

        <div id="canvas-wrapper">
            <canvas id="demo-canvas"></canvas>
        </div>

        <!-- Content -->
        <section id="content">

            <!-- Login Form -->
            <div class="allcp-form theme-primary mw320" id="login">
                <div class="bg-primary mw600 text-center mb20 br3 pt15 pb10">
                    <img src="assets/img/logo.png" alt=""/>
					<div class="sidebar-widget logo-widget">
					<div class="logo-slogan">
                            <div>LIMO INDO<span class="text-info">Fleet</span></div>
                        </div>
						</div>
                </div>
				
                <div class="panel mw320">
                    <form method="post" action="proses_login.php" id="form-login">
                        <div class="panel-body pn mv10">

                            <div class="section">
                                <label for="username" class="field prepend-icon">
                                    <input type="text" name="username" id="username" class="gui-input"
                                           placeholder="Username" required>
                                    <span class="field-icon">
                                        <i class="fa fa-user"></i>
                                    </span>
                                </label>
                            </div>
                            <!-- /section -->

                            <div class="section">
                                <label for="password" class="field prepend-icon">
                                    <input type="password" name="password" id="password" class="gui-input"
                                           placeholder="Password" required>
                                    <span class="field-icon">
                                        <i class="fa fa-lock"></i>
                                    </span>
                                </label>
                            </div>
                            <!-- /section -->

                            <div class="section">
                                <button type="submit" class="btn btn-bordered btn-primary pull-right">Log in</button>
                            </div>

                            <!-- /section -->

                        </div>
                        <!-- /Form -->
                    </form>
					<div class="section">
                    <a href="register.php"><button class="btn btn-bordered btn-primary pull-right">Register</button></a>
                    </div>
                </div>
                <!-- /Panel -->
            </div>
            <!-- /Spec Form -->

        </section>
        <!-- /Content -->

    </section>
    <!-- /Main Wrapper -->

</div>
<!-- /Body Wrap  -->



<!-- Scripts -->

<!-- jQuery -->
<script src="assets/js/jquery/jquery-1.12.3.min.js"></script>
<script src="assets/js/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- AnimatedSVGIcons -->
<script src="assets/fonts/animatedsvgicons/js/snap.svg-min.js"></script>
<script src="assets/fonts/animatedsvgicons/js/svgicons-config.js"></script>
<script src="assets/fonts/animatedsvgicons/js/svgicons.js"></script>
<script src="assets/fonts/animatedsvgicons/js/svgicons-init.js"></script>

<script src="assets/js/utility/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.concat.min.js"></script>


<script src="assets/js/plugins/highcharts/highcharts.js"></script>

<script src="assets/js/plugins/canvasbg/canvasbg.js"></script>

<script src="assets/js/utility/utility.js"></script>

<script src="assets/js/main.js"></script>
<script src="assets/js/demo/widgets_sidebar.js"></script>


<script src="assets/js/pages/dashboard_init.js"></script>
<script src="assets/js/demo/demo.js"></script>
</body>

</html>


