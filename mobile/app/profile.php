<?php
	include"../php/session_app.php";
	$id_c = $_SESSION['login']['id'];
	$a = mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM client WHERE id_user='".$id_c."'"));
	$b = mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM user WHERE fid_user='".$id_c."'"));
?>
<!DOCTYPE html>
<html lang="en-us" id="extr-page">
	<head>
		<meta charset="utf-8">
		<title> LimoIndoFleet</title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		
		<link rel="stylesheet" type="text/css" media="screen" href="../css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="../css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="../css/smartadmin-production-plugins.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="../css/smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="../css/smartadmin-skins.min.css">

		<link rel="stylesheet" type="text/css" media="screen" href="../css/smartadmin-rtl.min.css"> 

		<link rel="stylesheet" type="text/css" media="screen" href="css/demo.min.css">
		
		<link rel="stylesheet" type="text/css" href="../css/app.css">

		<!-- datepicker css -->
		<link href="jquery-ui-1.11.4/smoothness/jquery-ui.css" rel="stylesheet" />
		<link href="jquery-ui-1.11.4/jquery-ui.theme.css" rel="stylesheet" />
		
	</head>
	<body>
	
		<!-- side menu -->
		<div id="mySidenav" class="sidenav">
			<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
			<a href="./">Dashboard</a>
			<a href="profile.php">Profile</a>
		</div>
		
		<div class="row" style="margin-top:0px; margin-right:0px; margin-bottom:3px;">
			<div class="col-xs-9" style="background-color:#C0C0C0;">
				<span style="font-size:30px; cursor:pointer" onclick="openNav()">
					<i class="fa fa-navicon" style="margin-left:7px;"></i>
					<span style="color:#fff;">LimoIndoFleet</span>
				</span>
			</div>
			<div class="col-xs-3 pull-right" style="background-color:#C0C0C0; height:42px;">
				<a  style="cursor:pointer; color:#404040;" onClick="location.href='logout.php'" target="_top" title="Sign Out">
					<i class="fa fa-2x fa-sign-out pull-right" style="margin-top:7px;"></i>
				</a>
			</div>
		</div>

		<ul class="nav nav-tabs" style="margin-top:0px;">
			<li class="active"><a data-toggle="tab" href="#order"><i class="fa fa-user"></i> Profile</a></li>
			<li><a data-toggle="tab" href="#status"><i class="fa fa-lock"></i> Username & Password</a></li>
		</ul>
		<div class="tab-content">
			<div id="order" class="tab-pane fade in active" style="margin-top:7px; padding:17px;">
				<?php
					if(isset($_GET['info'])){
						echo $_GET['info'];
					}
				?>
				<div class="row">
					<div class="col-md-5 col-xs-12">
						<img src="../../assets/img/upload/<?php echo $a['foto_diri'];?>" width="100%" height="130">
					</div>	
				</div>
				<br/>
				<form name="profileedit" method="POST">
					<div class="row">
						<div class="col-md-5 col-xs-12">
							<label>Nama</label>
							<input type="text" name="txtidprofile" value="<?php echo $id_c;?>" class="hidden" />
							<input type="text" name="txtnama" value="<?php print $a['nama_user']; ?>" class="form-control" readonly />
						</div>
					</div>	
					<br/>
					<div class="row">
						<div class="col-md-5 col-xs-12">
							<label>Alamat</label>
							<input type="text" name="txtalamat" value="<?php print $a['alamat_user']; ?>" placeholder="Alamat" class="form-control" readonly />
						</div>
					</div>	
					<br/>
					<div class="row">
						<div class="col-md-5 col-xs-12">
							<label>No. Handphone</label>
							<input type="text" name="txtnohp" value="<?php print $a['telepon_user']; ?>" maxlength="13" placeholder="No. Handphone" class="form-control" onkeypress="return isNumberKeyTrue(event)" readonly />
						</div>
					</div>	
					<br/>
					<div class="row">
						<div class="col-md-5 col-xs-12">
							<label>Email</label>
							<input type="email" name="txtemail" value="<?php print $a['email_user']; ?>" placeholder="Alamat Email" class="form-control" readonly />
						</div>
					</div>	
					<br/>
					<div class="row">
						<div class="col-md-5 col-xs-12">
							<input type="submit" name="submit" value="Update" class="form-control btn btn-primary" />
						</div>
					</div>					
				</form>
				<br/><br/><br/><br/><br/>
			</div>
			<div id="status" class="tab-pane fade in" style="margin-top:7px; padding:17px;">
				<form name="useredit" method="POST">
					<div class="row">
						<div class="col-md-5 col-xs-12">
							<label>Username</label>
							<input type="text" name="txtiduser" value="<?php echo $id_c;?>" class="hidden" />
							<input type="text" name="txtuser" value="<?php print $b['username']; ?>" class="form-control" readonly />
						</div>
					</div>	
					<br/>
					<div class="row">
						<div class="col-md-5 col-xs-12">
							<label>Password</label>
							<input type="text" name="txtpass" value="<?php print $b['password']; ?>" placeholder="Password" class="form-control" required />
						</div>
					</div>	
					<br/>
					<div class="row">
						<div class="col-md-5 col-xs-12">
							<input type="submit" name="submit" value="Update" class="form-control btn btn-primary" />
						</div>
					</div>					
				</div>
				<br/><br/><br/><br/><br/>
			</div>	
		</div>

		<nav class="navbar-fixed-bottom" style="height:47px; background-color:#404040;">
			<div class="text-center" style="padding:5px;">
				<span style="color:#fff; font-size:11px;">
					Jl. Sidosermo Indah Raya No. 5, Surabaya<br>
					Email: limotransindo@gmail.com
				</span>
			</div>
		</nav>

	<?php
		if(isset($_POST['txtidprofile'])){
			
		}else{
			unset($_POST['txtidprofile']);
		}
		if(isset($_POST['txtiduser'])){
		}else{
			unset($_POST['txtiduser']);
		}
	?>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<script src="../js/bootstrap/bootstrap.min.js"></script>
		
		<script>
			function openNav() {
				document.getElementById("mySidenav").style.width = "250px";
			}

			function closeNav() {
				document.getElementById("mySidenav").style.width = "0";
			}
			
			//Hanya boleh Diisi dengan huruf
			function isNumberKey(evt){
				var charCode = (evt.which) ? evt.which : event.keyCode
				if ((charCode < 65) || (charCode == 32))
					return false;        
					return true;
			}
			//Hanya boleh Diisi dengan Dengan Angka
			function isNumberKeyTrue(evt){
				var charCode = (evt.which) ? evt.which : event.keyCode
				if (charCode > 65)
					return false;        
					return true;
			}
			function angka(e) {
				if (!/^[0-9]+$/.test(e.value)) {
				e.value = e.value.substring(0,e.value.length-1);
				}
			}	
			
		</script>

		
	</body>	
</html>