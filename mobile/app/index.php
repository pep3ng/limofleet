<?php
	include"../php/session_app.php";
	$id_c = $_SESSION['login']['id'];
	$a = mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM client WHERE id_user='".$id_c."'"));
?>
<!DOCTYPE html>
<html lang="en-us" id="extr-page">
	<head>
		<meta charset="utf-8">
		<title> LimoIndoFleet</title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		
		<link rel="stylesheet" type="text/css" media="screen" href="../css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="../css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="../css/smartadmin-production-plugins.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="../css/smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="../css/smartadmin-skins.min.css">

		<link rel="stylesheet" type="text/css" media="screen" href="../css/smartadmin-rtl.min.css"> 

		<link rel="stylesheet" type="text/css" media="screen" href="css/demo.min.css">
		
		<link rel="stylesheet" type="text/css" href="../css/app.css">

		<!-- datepicker css -->
		<link href="jquery-ui-1.11.4/smoothness/jquery-ui.css" rel="stylesheet" />
		<link href="jquery-ui-1.11.4/jquery-ui.theme.css" rel="stylesheet" />
		
	</head>
	<body>
	
		<!-- side menu -->
		<div id="mySidenav" class="sidenav">
			<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
			<a href="./">Dashboard</a>
			<a href="profile.php">Profile</a>
		</div>
		
		<div class="row" style="margin-top:0px; margin-right:0px; margin-bottom:3px;">
			<div class="col-xs-9" style="background-color:#C0C0C0;">
				<span style="font-size:30px; cursor:pointer" onclick="openNav()">
					<i class="fa fa-navicon" style="margin-left:7px;"></i>
					<span style="color:#fff;">LimoIndoFleet</span>
				</span>
			</div>
			<div class="col-xs-3 pull-right" style="background-color:#C0C0C0; height:42px;">
				<a  style="cursor:pointer; color:#404040;" onClick="location.href='logout.php'" target="_top" title="Sign Out">
					<i class="fa fa-2x fa-sign-out pull-right" style="margin-top:7px;"></i>
				</a>
			</div>
		</div>

		<ul class="nav nav-tabs" style="margin-top:0px;">
			<li class="active"><a data-toggle="tab" href="#order"><i class="fa fa-shopping-cart"></i> Order</a></li>
			<li><a data-toggle="tab" href="#status"><i class="fa fa-tags"></i> Status</a></li>
		</ul>
		<div class="tab-content">
			<div id="order" class="tab-pane fade in active" style="margin-top:7px; padding:17px;">
				<?php
					if(isset($_GET['info'])){
						echo $_GET['info'];
					}
				?>
				<div class="row">
					<div class="col-md-5 col-xs-12">
						<label>Lokasi Anda</label>
						<div id="map" style="width:100%;height:170px;"></div>				
					</div>
				</div>
				<br/>
				<form name="login" id="myform" method="post" action="simpan-order.php" enctype="multipart/form-data">
					<div class="row">
						<div class="col-md-5 col-xs-12">
							<input type="text" name="txtidclient" value="<?php echo $id_c;?>" class="hidden" />
							<label>Pilih Vendor</label>
							<select class="form-control" name="txtperusahaan" id="rekanan" required>
								<option></option>
								<?php
									$dat = "SELECT * FROM perusahaan ORDER BY id_perusahaan ASC";
									$que = mysqli_query($conn,$dat);
									while($hsl=mysqli_fetch_array($que)){
										echo"
											<option value=\"".$hsl['id_perusahaan']."\">".$hsl['perusahaan']."</option>
										";
									}
								?>
							</select>											
						</div>
					</div>
					<br/>
					<div class="row">
						<div class="col-md-5 col-xs-12">
							<label>Pilih Kendaraan</label>
							<select class="form-control" name="txtkendaraan" id="kendaraan" required>
								<option></option>
								<?php
									$dat = "SELECT * FROM kendaraan WHERE status_kendaraan='1' ORDER BY id_kendaraan ASC";
									$que = mysqli_query($conn,$dat);
									while($hsl=mysqli_fetch_array($que)){
										$i = mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM merk_kendaraan WHERE id_merk='".$hsl['fid_merk']."'"));
										echo"
											<option value=\"".$hsl['id_kendaraan']."\">".$hsl['nopol_kendaraan']." | ".$i['produsen_kendaraan']." ".$i['merk_kendaraan']."</option>
										";
									}
								?>
							</select>											
						</div>
					</div>
					<br/>	
					<div class="row">					
						<div class="col-md-5 col-xs-12">
							<label>Jenis Sewa</label>
							<select class="form-control" name="txtjnssewa" id="jenissewa" required />
								<option></option>
								<option value="1">Dengan Driver</option>
								<option value="2">Lepas Kunci</option>
							</select>
						</div>
					</div>
					<br/>				
					<div id="jaminan"></div>	
					<div class="row">					
						<div class="col-md-5 col-xs-12">
							<label>Tanggal Sewa</label>
							<input type="text" name="txttgl1" class="form-control" id="tanggal" required /><br/>
						</div>
					</div>
					<div class="row">					
						<div class="col-md-5 col-xs-12">
							<label>Tanggal Kembali</label>
							<input type="text" name="txttgl2" class="form-control" id="tanggal1" required /><br/>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-5 col-xs-12">
							<input type="submit" name="submit" value="Order Sekarang" class="form-control btn btn-primary" />
						</div>
					</div>
				</form>
				<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
			</div>
			<div id="status" class="tab-pane fade in" style="margin-top:7px; padding:17px;">
				<table class="table table-hover" width="100%">
					<thead>
						<tr>
							<th width="70%">Info Order</th>
							<th width="30%">Aksi Order</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$dat = "SELECT * FROM data_order WHERE fid_user='".$id_c."' ORDER BY id_order DESC";
							$que = mysqli_query($conn,$dat);
							while($c=mysqli_fetch_array($que)){	
								$tgl_b = implode('',explode('-',$c['tanggal_berangkat']));
								$tgl_k = implode('',explode('-',$c['tanggal_datang']));
								if($c['status_order']=='1'){$status = 'Pending';}
								elseif($c['status_order']=='2'){$status = 'Aktif';}
								elseif($c['status_order']=='3'){$status = 'Selesai';}
								$d = mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM perusahaan WHERE id_perusahaan='".$c['fid_perusahaan']."'"));
								$e = mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM kendaraan WHERE id_kendaraan='".$c['fid_kendaraan']."'"));
								$f = mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM merk_kendaraan WHERE id_merk='".$e['fid_merk']."'"));
						?>
						<tr>
							<td width="70%">
								Order ID: <?php echo $c['id_order'];?><br/>
								Perusahaan Rental: <?php echo $d['perusahaan'];?><br/>
								Alamat Rental: <?php echo $d['alamat_perusahaan'];?><br/>
								Mobil: <?php echo $f['produsen_kendaraan'];?> <?php echo $f['merk_kendaraan'];?><br/>
								Nopol: <?php echo $e['nopol_kendaraan'];?><br/>
								Tanggal Berangkat: <?php echo substr($tgl_b,6,2)."-".substr($tgl_b,4,2)."-".substr($tgl_b,0,4);?><br/>
								Jam Berangkat: <?php echo $c['jam_berangkat'];?><br/>
								Tanggal Kembali: <?php echo substr($tgl_k,6,2)."-".substr($tgl_k,4,2)."-".substr($tgl_k,0,4);?><br/>
								Jam Kembali: <?php echo $c['jam_datang'];?><br/>
								KM Awal: <?php echo $c['km_awal'];?><br/>
								KM Akhir: <?php echo $c['km_akhir'];?><br/>
								Jaminan: <?php echo $c['jenis_jaminan'];?><br/>
								Keterangan Jaminan: <?php echo $c['info_jaminan'];?><br/>
							</td>
							<td width="30%">
								Status: <?php echo $status;?><br/>
							
							</td>
						<?php
							}
						?>
						</tr>
					</tbody>
				</table>
				<br/><br/><br/><br/><br/><br/><br/><br/>
			</div>	
		</div>

		<nav class="navbar-fixed-bottom" style="height:47px; background-color:#404040;">
			<div class="text-center" style="padding:5px;">
				<span style="color:#fff; font-size:11px;">
					Jl. Sidosermo Indah Raya No. 5, Surabaya<br>
					Email: limotransindo@gmail.com
				</span>
			</div>
		</nav>		
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<script src="../js/bootstrap/bootstrap.min.js"></script>
		<!-- datepicker -->
		<script src="jquery-ui-1.11.4/jquery-ui.js"></script>
		<script src="jquery-ui-1.11.4/jquery-ui.min.js"></script>

		<script>
		// Note: This example requires that you consent to location sharing when
		// prompted by your browser. If you see the error "The Geolocation service
		// failed.", it means you probably did not give permission for the browser to
		// locate you.
		var map, infoWindow;
		function initMap() {
			map = new google.maps.Map(document.getElementById('map'), {
			center: {lat: -34.397, lng: 150.644},
			zoom: 15
			});
			infoWindow = new google.maps.InfoWindow;

			// Try HTML5 geolocation.
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(function(position) {
					var pos = {
						lat: position.coords.latitude,
						lng: position.coords.longitude
					};

					infoWindow.setPosition(pos);
					infoWindow.setContent('Posisi Anda.');
					infoWindow.open(map);
					map.setCenter(pos);
				}, function() {
					handleLocationError(true, infoWindow, map.getCenter());
				});
			} else {
			// Browser doesn't support Geolocation
			handleLocationError(false, infoWindow, map.getCenter());
			}
		}

		function handleLocationError(browserHasGeolocation, infoWindow, pos) {
			infoWindow.setPosition(pos);
			infoWindow.setContent(browserHasGeolocation ?
                              'Error: Aktifkan Permission Get Location Info di Setting.' :
                              'Error: Smartphone anda tidak mendukung aplikasi ini.');
			infoWindow.open(map);
		}
		</script>
		<script async defer
			src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDn-LLwACxbTzOTts4GlSc-EhEQ41k-9lw&callback=initMap">
		</script>
		
		<!-- datepicker -->
		<script>
			$(document).ready(function(){
				$("#tanggal").datepicker({
				})
			})
			$(document).ready(function(){
				$("#tanggal1").datepicker({
				})
			})
		</script>
		
		<script>
			function openNav() {
				document.getElementById("mySidenav").style.width = "250px";
			}

			function closeNav() {
				document.getElementById("mySidenav").style.width = "0";
			}
		</script>

		<script type="text/javascript">
			var htmlobjek;
			$(document).ready(function(){
				//apabila terjadi event onchange terhadap object <select id=propinsi>
				$("#rekanan").change(function(){
					var rekanan = $("#rekanan").val();
					$.ajax({
						url: "kendaraan.php",
						data: "rekanan="+rekanan,
						cache: false,
						success: function(msg){
						//jika data sukses diambil dari server kita tampilkan
						//di <select id=kota>
						$("#kendaraan").html(msg);
						}
					});
				});
			});
		</script>			

		<script type="text/javascript">
			var htmlobjek;
			$(document).ready(function(){
				//apabila terjadi event onchange terhadap object <select id=propinsi>
				$("#jenissewa").change(function(){
					var jenissewa = $("#jenissewa").val();
					$.ajax({
						url: "jaminan.php",
						data: "jenissewa="+jenissewa,
						cache: false,
						success: function(msg){
						//jika data sukses diambil dari server kita tampilkan
						//di <select id=kota>
						$("#jaminan").html(msg);
						}
					});
				});
			});
		</script>			
		
	</body>	
</html>