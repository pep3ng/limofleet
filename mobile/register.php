
<!DOCTYPE html>
<html lang="en-us" id="extr-page">
	<head>
		<meta charset="utf-8">
		<title> LimoIndoFleet</title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		
		<link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="css/font-awesome.min.css">

		<link rel="stylesheet" type="text/css" media="screen" href="css/smartadmin-production-plugins.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="css/smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="css/smartadmin-skins.min.css">

		<link rel="stylesheet" type="text/css" media="screen" href="css/smartadmin-rtl.min.css"> 

		<link rel="stylesheet" type="text/css" media="screen" href="css/demo.min.css">

		<link rel="shortcut icon" href="img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="img/favicon/favicon.ico" type="image/x-icon">

		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

		<link rel="apple-touch-icon" href="img/splash/sptouch-icon-iphone.png">
		<link rel="apple-touch-icon" sizes="76x76" href="img/splash/touch-icon-ipad.png">
		<link rel="apple-touch-icon" sizes="120x120" href="img/splash/touch-icon-iphone-retina.png">
		<link rel="apple-touch-icon" sizes="152x152" href="img/splash/touch-icon-ipad-retina.png">
		
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		
		<link rel="apple-touch-startup-image" href="img/splash/ipad-landscape.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
		<link rel="apple-touch-startup-image" href="img/splash/ipad-portrait.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
		<link rel="apple-touch-startup-image" href="img/splash/iphone.png" media="screen and (max-device-width: 320px)">

	</head>
	<body id="login">
		<header id="header">

			<div id="logo-group">
				<span id="logo"> <img src="img/logo-limo.png" alt="LimoIndoFleet"> </span>

				<!-- END AJAX-DROPDOWN -->
			</div>

			<span id="extr-page-header-space"> <span class="hidden-mobile hidden-xs">Sudah mendaftar?</span> <a href="./" class="btn btn-danger">Sign In</a> </span>

		</header>

		<div id="main" role="main">

			<!-- MAIN CONTENT -->
			<div id="content" class="container">

				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-7 col-lg-7 hidden-xs hidden-sm">
						<h1 class="txt-color-red login-header-big">LimoIndoFleet</h1>
						<div class="hero">

							<div class="pull-left login-desc-box-l">
								<h4 class="paragraph-header">Rasakan kemudahan menggunakan LimoIndoFleet, dimanapun anda berada!</h4>
								<div class="login-app-icons">
									<a style="cursor:pointer" onClick="location.href='www.limoindo.com'" class="btn btn-danger btn-sm">Website LimoIndoFleet</a>
								</div>
							</div>
							
							<img src="img/demo/iphoneview.png" alt="" class="pull-right display-image" style="width:210px">
							
						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<h5 class="about-heading">Update LimoIndoFleet anda !</h5>
								<p>
									Update aplikasi mobile LimoIndoFleet anda di playstore dan appstore.
								</p>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<h5 class="about-heading">Simple but Powerfull !</h5>
								<p>
									LimoIndoFleet adalah aplikasi Management Kendaraan yang Simple tapi Powerfull.
								</p>
							</div>
						</div>

					</div>
					<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
						<div class="well no-padding">

							<form action="php/proses-daftar.php" method="post" id="smart-form-register" class="smart-form client-form" enctype="multipart/form-data">
							
								<header>
									Pendaftaran GRATIS*
								</header>

								<fieldset>
								
								<?php
									if(isset($_GET['info'])){
										echo $_GET['info'];
									}
								?>
									<section>
										<label>Lokasi Anda</label>
										<div id="map" style="width:100%;height:170px;"></div>
									</section>
									<section>
										<label class="input"> <i class="icon-append fa fa-phone"></i>
											<input type="text" name="nohp" placeholder="No. Handphone">
											<b class="tooltip tooltip-bottom-right">Pengenalan No. Handphone</b> </label>
									</section>
								
									<section>
										<label class="input"> <i class="icon-append fa fa-user"></i>
											<input type="text" name="username" placeholder="Username">
											<b class="tooltip tooltip-bottom-right">Username untuk Login</b> </label>
									</section>

									<section>
										<label class="input"> <i class="icon-append fa fa-envelope"></i>
											<input type="email" name="email" placeholder="Email address">
											<b class="tooltip tooltip-bottom-right">Password untuk Login</b> </label>
									</section>

									<section>
										<label class="input"> <i class="icon-append fa fa-lock"></i>
											<input type="password" name="password" placeholder="Password" id="password">
											<b class="tooltip tooltip-bottom-right">Jangan Lupa password anda ya</b> </label>
									</section>

									<section>
										<label class="input"> <i class="icon-append fa fa-lock"></i>
											<input type="password" name="passwordConfirm" placeholder="Confirm password">
											<b class="tooltip tooltip-bottom-right">Jangan Lupa password anda ya</b> </label>
									</section>
								</fieldset>

								<fieldset>
									<div class="row">
										<section class="col col-6">
											<label class="input">
												<input type="text" name="firstname" placeholder="First name">
											</label>
										</section>
										<section class="col col-6">
											<label class="input">
												<input type="text" name="lastname" placeholder="Last name">
											</label>
										</section>
									</div>

									<div class="row">
										<section class="col col-6">
											<label class="input">
												<input type="text" name="alamat" placeholder="Alamat">
											</label>
										</section>									
										<section class="col col-6">
											<label class="select">
												<select name="gender">
													<option value="0" selected="" disabled="">Gender</option>
													<option value="Bp.">Laki-Laki</option>
													<option value="Ibu.">Perempuan</option>
												</select> <i></i> </label>
										</section>
									</div>

									<div class="row">
										<section class="col col-6">
											<label>Upload Foto Diri</label>
											<input type="file" name="txtfoto" accept="image/*" class="btn btn-default" id="exampleInputFile1">
											<p class="help-block">
												Ukuran file foto tidak boleh lebih dari 200kB.
											</p>
										</section>									
									</div>
									
									<div class="row">
										<section class="col col-6">
											<label>Upload Foto KTP</label>
											<input type="file" name="txtktp" accept="image/*" class="btn btn-default" id="exampleInputFile1">
											<p class="help-block">
												Ukuran file foto tidak boleh lebih dari 200kB.
											</p>											
										</section>
									</div>

									<div class="row">
										<section class="col col-6">
											<label>Upload Foto SIM A</label>
											<input type="file" name="txtsim" accept="image/*" class="btn btn-default" id="exampleInputFile1">
											<p class="help-block">
												Ukuran file foto tidak boleh lebih dari 200kB.
											</p>																						
										</section>
									</div>

									<div class="row">
										<section class="col col-6">
											<label>Upload Foto Kartu Keluarga</label>
											<input type="file" name="txtkk" accept="image/*" class="btn btn-default" id="exampleInputFile1">
											<p class="help-block">
												Ukuran file foto tidak boleh lebih dari 200kB.
											</p>																																	
										</section>																			
									</div>

									<section>
										<label class="checkbox">
											<input type="checkbox" name="subscription" id="subscription">
											<i></i>Saya ingin menerima berita dan penawaran baru</label>
										<label class="checkbox">
											<input type="checkbox" name="terms" id="terms">
											<i></i>Saya setuju dengan <a href="#" data-toggle="modal" data-target="#myModal"> Syarat dan Kondisi </a></label>
									</section>
								</fieldset>
								<footer>
									<button type="submit" class="btn btn-primary">
										Daftar
									</button>
								</footer>

								<div class="message">
									<i class="fa fa-check"></i>
									<p>
										Terima Kasih telah mendaftar!
									</p>
								</div>
							</form>

						</div>
						
						<h5 class="text-center">- Kantor Kami -</h5>							
						<div class="text-center">
							Jl. Sidosermo Indah Raya No. 5, Surabaya<br/>
							Telp: 031-8410214, 087853829500, 085645683353<br/>
							WhatsApp: 082142600062<br/>
							Email: kanigaratrans@yahoo.co.id	
						</div>
					</div>
				</div>
			</div>

		</div>

		<!-- Modal -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							&times;
						</button>
						<h4 class="modal-title" id="myModalLabel">Terms & Conditions</h4>
					</div>
					<div class="modal-body custom-scroll terms-body">
						
 <div id="left">


            <h2>Registrations and authorisations</h2>

            <p>[No. Handphone adalah No. Handphone anda sendiri.]</p>

            <p>[Foto Diri, Foto KTP, Foto SIM A dan Foto Kartu Keluarga yang anda upload adalah hak milik anda pribadi, bukan hak milik orang lain.]</p>

            <p>[Apabila suatu hari ada tuntutan dari pihak lain atas data-data yang anda upload, kami sebagai penyedia aplikasi tidak bertanggungjawab.]</p>

            <p>[bila ada pertanyaan silahkan hubungi contact dibawah form ini]</p>


            </div>
			

					</div>
					<div class="modal-footer">
					
						<button type="button" class="btn btn-default" data-dismiss="modal">
							Batal
						</button>
						<button type="button" class="btn btn-primary" id="i-agree">
							<i class="fa fa-check"></i> Saya Setuju
						</button>
						
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

		<!--================================================== -->	

		<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
		<script src="js/plugin/pace/pace.min.js"></script>

	    <!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
	    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<script> if (!window.jQuery) { document.write('<script src="js/libs/jquery-2.1.1.min.js"><\/script>');} </script>

	    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
		<script> if (!window.jQuery.ui) { document.write('<script src="js/libs/jquery-ui-1.10.3.min.js"><\/script>');} </script>

		<!-- IMPORTANT: APP CONFIG -->
		<script src="js/app.config.js"></script>

		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events 		
		<script src="js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> -->

		<!-- BOOTSTRAP JS -->		
		<script src="js/bootstrap/bootstrap.min.js"></script>

		<!-- JQUERY VALIDATE -->
		<script src="js/plugin/jquery-validate/jquery.validate.min.js"></script>
		
		<!-- JQUERY MASKED INPUT -->
		<script src="js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		
		<!--[if IE 8]>
			
			<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>
			
		<![endif]-->

		<!-- MAIN APP JS FILE -->
		<script src="js/app.min.js"></script>

		<script>
		// Note: This example requires that you consent to location sharing when
		// prompted by your browser. If you see the error "The Geolocation service
		// failed.", it means you probably did not give permission for the browser to
		// locate you.
		var map, infoWindow;
		function initMap() {
			map = new google.maps.Map(document.getElementById('map'), {
			center: {lat: -34.397, lng: 150.644},
			zoom: 15
			});
			infoWindow = new google.maps.InfoWindow;

			// Try HTML5 geolocation.
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(function(position) {
					var pos = {
						lat: position.coords.latitude,
						lng: position.coords.longitude
					};

					infoWindow.setPosition(pos);
					infoWindow.setContent('Posisi Anda.');
					infoWindow.open(map);
					map.setCenter(pos);
				}, function() {
					handleLocationError(true, infoWindow, map.getCenter());
				});
			} else {
			// Browser doesn't support Geolocation
			handleLocationError(false, infoWindow, map.getCenter());
			}
		}

		function handleLocationError(browserHasGeolocation, infoWindow, pos) {
			infoWindow.setPosition(pos);
			infoWindow.setContent(browserHasGeolocation ?
                              'Error: Aktifkan Permission Get Location Info di Setting.' :
                              'Error: Smartphone anda tidak mendukung aplikasi ini.');
			infoWindow.open(map);
		}
		</script>
		<script async defer
			src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDn-LLwACxbTzOTts4GlSc-EhEQ41k-9lw&callback=initMap">
		</script>
		
		<script type="text/javascript">
			runAllForms();
			
			// Model i agree button
			$("#i-agree").click(function(){
				$this=$("#terms");
				if($this.checked) {
					$('#myModal').modal('toggle');
				} else {
					$this.prop('checked', true);
					$('#myModal').modal('toggle');
				}
			});
			
			// Validation
			$(function() {
				// Validation
				$("#smart-form-register").validate({

					// Rules for form validation
					rules : {
						nohp : {
							required : true,
							number: true,
							minlength:11,
							maxlength:13
						},						
						username : {
							required : true
						},
						email : {
							required : true,
							email : true
						},
						password : {
							required : true,
							minlength : 3,
							maxlength : 20
						},
						passwordConfirm : {
							required : true,
							minlength : 3,
							maxlength : 20,
							equalTo : '#password'
						},
						firstname : {
							required : true
						},
						lastname : {
							required : true
						},
						alamat : {
							required : true
						},						
						gender : {
							required : true
						},
						terms : {
							required : true
						},
						txtfoto: {
							required : true
						},
						txtktp: {
							required : true
						},
						txtsim: {
							required : true
						},
						txtkk: {
							required : true
						}
						
					},

					// Messages for form validation
					messages : {
						nohp : {
							required : 'Tolong isi No. Handphone anda',
							number: 'Tolong isi No. Handphone yang benar'
						},						
						username : {
							required : 'Tolong isi Username anda'
						},
						email : {
							required : 'Tolong isi alamat email anda',
							email : 'Tolong isi alamat email yang benar'
						},
						password : {
							required : 'Tolong isi password anda'
						},
						passwordConfirm : {
							required : 'Tolong isi password anda lagi',
							equalTo : 'Tolong isi password yang sama dengan diatas'
						},
						firstname : {
							required : 'Tolong isi nama depan anda'
						},
						lastname : {
							required : 'Tolong isi nama belakang anda'
						},
						alamat : {
							required : 'Tolong isi alamat anda'
						},						
						gender : {
							required : 'Tolong pilih gender anda'
						},
						terms : {
							required : 'Anda harus setuju dengan Syarat dan Kondisi'
						},
						txtfoto : {
							required : 'Tolong pilih Foto anda'
						},
						txtktp : {
							required : 'Tolong pilih Foto KTP anda'
						},
						txtsim : {
							required : 'Tolong pilih Foto SIM A anda'
						},
						txtkk : {
							required : 'Tolong pilih Foto Kartu Keluarga anda'
						}						
					},

					// Ajax form submition
					submitHandler : function(form) {
						$(form).ajaxSubmit({
							success : function() {
								$("#smart-form-register").addClass('submited');
							}
						});
					},

					// Do not change code below
					errorPlacement : function(error, element) {
						error.insertAfter(element.parent());
					}
				});

			});
		</script>
		
</html>