<!doctype html>
<html>
  <head>
    <title>Google Maps Example</title>
    <script src="https://cdn.pubnub.com/sdk/javascript/pubnub.4.4.1.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" />
  </head>
  <body>
    <div class="container">
      <h1>PubNub Google Maps Example</h1>
      <div id="map-canvas" style="width:100%;height:600px"></div>
    </div>

    <script>
    window.lat = 37.8199;
    window.lng = -122.4783;

    var map;
    var mark;
    var lineCoords = [];

	navigator.geolocation.getCurrentPosition(function(position) {
		window.lat = position.coords.latitude;
		window.lng = position.coords.longitude;
	});
	
    var initialize = function() {
      map  = new google.maps.Map(document.getElementById('map-canvas'), {center:{lat:lat,lng:lng},zoom:15});
      mark = new google.maps.Marker({position:{lat:lat, lng:lng}, map:map});
      lineCoords.push(new google.maps.LatLng(window.lat, window.lng));
    };

    window.initialize = initialize;

    var redraw = function(payload) {
      lat = payload.message.lat;
      lng = payload.message.lng;

      map.setCenter({lat:lat, lng:lng, alt:0});
      mark.setPosition({lat:lat, lng:lng, alt:0});
      lineCoords.push(new google.maps.LatLng(lat, lng));

      var lineCoordinatesPath = new google.maps.Polyline({
        path: lineCoords,
        geodesic: true,
        strokeColor: '#2E10FF'
      });
      
      lineCoordinatesPath.setMap(map);
    };

    var pnChannel = "map-channel";

    var pubnub = new PubNub({
      publishKey: 'pub-c-d1c22f45-20ab-4712-b1e0-3782c321ad63',
      subscribeKey: 'sub-c-d086ac3a-686b-11e7-b6db-02ee2ddab7fe'
    });

    pubnub.subscribe({channels: [pnChannel]});
    pubnub.addListener({message:redraw});

    setInterval(function() {
      pubnub.publish({channel:pnChannel, message:{lat:window.lat + 0.001, lng:window.lng + 0.01}});
    }, 5000);
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDn-LLwACxbTzOTts4GlSc-EhEQ41k-9lw&callback=initialize"></script>
  </body>
</html>