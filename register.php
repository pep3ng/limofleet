<!DOCTYPE html>
<html>
<?php include "header.php"; ?>

<body class="utility-page sb-l-c sb-r-c">


<!-- Body Wrap -->
<div id="main" class="animated fadeIn">

    <!-- Main Wrapper -->
    <section id="content_wrapper">

        <div id="canvas-wrapper">
            <canvas id="demo-canvas"></canvas>
        </div>

        <!-- Content -->
        <section id="content" class="">

            <!-- Registration -->
            <div class="allcp-form theme-primary mw600" id="register">
                <div class="bg-primary mw600 text-center mb20 br3 pt15 pb10">
                    <img src="assets/img/logo.png" alt=""/>
                </div>
                <div class="panel panel-primary">
                    <div class="panel-heading pn">
                                    <span class="panel-title">
                                      FORM PENDAFTARAN
                                    </span>
                    </div>
                    <!-- /Panel Heading -->

                    <form method="post" action="simpan_pendaftaran.php" id="form-register" enctype="multipart/form-data">
                        <div class="panel-body pn">
                            <div class="section">
                                 <label for="nama_user" class="field prepend-icon">
                                    <input type="text" name="nama_user" id="nama_user" class="gui-input"
                                           placeholder="Nama Lengkap" required>
                                    <span class="field-icon">
                                        <i class="fa fa-user"></i>
                                    </span>
                                </label>
                            </div>
                             <div class="section">
                                 <label for="alamat_user" class="field prepend-icon">
                                    <input type="text" name="alamat_user" id="alamat_user" class="gui-input"
                                           placeholder="Alamat Lengkap" required>
                                    <span class="field-icon">
                                        <i class="fa fa-home"></i>
                                    </span>
                                </label>
                            </div>
							<div class="section">
                                 <label for="telepon_user" class="field prepend-icon">
                                    <input type="text" name="telepon_user" id="telepon_user" class="gui-input"
                                           placeholder="Telepon" required>
                                    <span class="field-icon">
                                        <i class="fa fa-phone"></i>
                                    </span>
                                </label>
                            </div>
                            <div class="section">
                                <label for="email_user" class="field prepend-icon">
                                    <input type="email_user" name="email_user" id="email_user" class="gui-input"
                                           placeholder="Email" required>
                                    <span class="field-icon">
                                        <i class="fa fa-envelope"></i>
                                    </span>
                                </label>
                            </div>
                            <!-- /section -->

                            <div class="section">
                                <label for="username" class="field prepend-icon">
                                    <input type="text" name="username" id="username" class="gui-input"
                                           placeholder="username" required>
                                    <span class="field-icon">
                                        <i class="fa fa-user"></i>
                                    </span>
                                </label>
                            </div>
                            <!-- /section -->

                            <div class="section">
                                <label for="password" class="field prepend-icon">
                                    <input type="password" name="password" id="password" class="gui-input"
                                           placeholder="Password" required>
                                    <span class="field-icon">
                                        <i class="fa fa-lock"></i>
                                    </span>
                                </label>
                            </div>
							<div class="section">
							<label class="col-sm-2 control-label">Foto Diri:</label>
                                <label for="foto_diri" class="field prepend-icon">
                                    <input type="file" name="foto_diri" id="foto_diri" class="gui-input"
                                          >
                                    <span class="field-icon">
                                        <i class="fa fa-file"></i>
                                    </span>
                                </label>
                            </div>
							<div class="section">
							<label class="col-sm-2 control-label">Foto KTP:</label>
                                <label for="foto_ktp" class="field prepend-icon">
                                    <input type="file" name="foto_ktp" id="foto_ktp" class="gui-input"
                                          >
                                    <span class="field-icon">
                                        <i class="fa fa-file"></i>
                                    </span>
                                </label>
                            </div>
							<div class="section">
							<label class="col-sm-2 control-label">Foto SIM A:</label>
                                <label for="foto_sima" class="field prepend-icon">
                                    <input type="file" name="foto_sima" id="foto_sima" class="gui-input"
                                          >
                                    <span class="field-icon">
                                        <i class="fa fa-file"></i>
                                    </span>
                                </label>
                            </div>
							<div class="section">
							<label class="col-sm-2 control-label">Foto KSK:</label>
                                <label for="foto_ksk" class="field prepend-icon">
                                    <input type="file" name="foto_ksk" id="foto_ksk" class="gui-input"
                                          >
                                    <span class="field-icon">
                                        <i class="fa fa-file"></i>
                                    </span>
                                </label>
                            </div>
                            <!-- /section -->

                            <div class="section">
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-bordered btn-primary">I Accept - Create Account
                                    </button>
                                </div>
                            </div>
                            <!-- /section -->

                        </div>
                        <!-- /Form -->
                        <div class="panel-footer">

                        </div>
                        <!-- /Panel Footer -->
                    </form>
                </div>
            </div>
            <!-- /Spec Form -->

        </section>
        <!-- /Content -->

    </section>
    <!-- /Main Wrapper -->

</div>


<!-- jQuery -->
<script src="assets/js/jquery/jquery-1.12.3.min.js"></script>
<script src="assets/js/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- AnimatedSVGIcons -->
<script src="assets/fonts/animatedsvgicons/js/snap.svg-min.js"></script>
<script src="assets/fonts/animatedsvgicons/js/svgicons-config.js"></script>
<script src="assets/fonts/animatedsvgicons/js/svgicons.js"></script>
<script src="assets/fonts/animatedsvgicons/js/svgicons-init.js"></script>

<!-- Scroll -->
<script src="assets/js/utility/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.concat.min.js"></script>




<!-- HighCharts Plugin -->
<script src="assets/js/plugins/highcharts/highcharts.js"></script>





<!-- CanvasBG JS -->
<script src="assets/js/plugins/canvasbg/canvasbg.js"></script>
<script src="assets/js/demo/demo.js"></script>
<!-- Theme Scripts -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/main.js"></script>
<script src="assets/js/demo/widgets_sidebar.js"></script>









































<script src="assets/js/pages/dashboard_init.js"></script>

<!-- /Scripts -->

</body>


<!-- Mirrored from admink.themerex.net/utility-register.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 05 Jul 2017 05:08:42 GMT -->
</html>


