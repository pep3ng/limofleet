<?php include "session_cek.php" ?>
<html>
<?php include "header_admin.php"; ?>
    <!-- Daterange CSS -->
    <link rel="stylesheet" type="text/css" href="assets/js/plugins/daterange/daterangepicker.css">

    <!-- Tagmanager CSS -->
    <link rel="stylesheet" type="text/css" href="assets/js/plugins/tagmanager/tagmanager.css">

    <!-- Datetimepicker CSS -->
    <link rel="stylesheet" type="text/css" href="assets/js/plugins/datepicker/css/bootstrap-datetimepicker.css">
    <!-- Main Wrapper -->
    <section id="content_wrapper">

        <!-- Content -->
        <section id="content" class="animated fadeIn">

            <div class="row">
                <div class="col-md-12 mb40">

                    <div class="tab-block">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab1" data-toggle="tab">Edit Order</a>
                            </li>
                        </ul>
                        <div class="tab-content p30">
                            <div id="tab1" class="tab-pane active">
                    <div class="panel">
                        <div class="panel-heading">
                            <span class="panel-title">Edit Order</span>
                        </div>
                        <div class="panel-body">
										<?php 
						include ("db.php");
						$id=$_GET['id'];
						$sql = mysqli_query($koneksi,"select * from data_order,kendaraan,merk_kendaraan
									WHERE data_order.fid_merk=merk_kendaraan.id_merk
									AND data_order.status_order=1
									AND id_order='$id'") or die("error sql");
						while($row = mysqli_fetch_assoc($sql)){
							$km_awal=$row['km_awal'];
							$km_akhir=$row['km_akhir'];
							$jam_berangkat=$row['jam_berangkat'];
							$harga_sewa=$row['harga_sewa'];
							$fid_perusahaan=$row['fid_perusahaan'];
							$fid_merk=$row['fid_merk'];
						}
						?> 
                   <form class="form-horizontal" method="post" action="update_order.php">
							<div class="form-group">
                                    <label for="inputStandard" class="col-lg-3 control-label">Pilih Kendaraan:</label>
                                    <div class="col-lg-8">
                                        <div class="bs-component">
                                            <select name="fid_kendaraan" class="form-control">
													<option value="0">-Pilih Kendaraan-</option>
													<?php 
													$sql = mysqli_query($koneksi,"SELECT * from merk_kendaraan,kendaraan where merk_kendaraan.fid_perusahaan='$fid_perusahaan' AND kendaraan.fid_merk=merk_kendaraan.id_merk AND kendaraan.fid_merk='$fid_merk' AND kendaraan.status_kendaraan=1") or die("error sql");
													while($row = mysqli_fetch_assoc($sql)){
													?>
													
													<option value="<?php echo $row['id_kendaraan'];?>"><?php echo $row['merk_kendaraan'];echo "&nbsp"; echo $row['nopol_kendaraan']; ?></option>
													<?php } ?>
													</select>
                                        </div>
                                    </div>
                                </div>
								<div class="form-group">
                                    <label for="inputStandard" class="col-lg-3 control-label">KM Awal</label>
                                    <div class="col-lg-8">
                                        <div class="bs-component">
                                            <input type="text" id="km_awal" name="km_awal" class="form-control"
                                                   value="<?php echo $km_awal;?>">
                                        </div>
                                    </div>
                                </div>
								<div class="form-group">
                                    <label for="inputStandard" class="col-lg-3 control-label">KM Akhir</label>
                                    <div class="col-lg-8">
                                        <div class="bs-component">
                                            <input type="text" id="km_akhir" name="km_akhir" class="form-control"
                                                    value="<?php echo $km_akhir;?>">
                                        </div>
                                    </div>
                                </div>
								<div class="form-group">
                                    <label for="inputStandard" class="col-lg-3 control-label">Jam Berangkat</label>
                                    <div class="col-lg-8">
                                        <div class="input-group date" id="datetimepicker6">
                                            <span class="input-group-addon cursor">
                                              <i class="glyphicon glyphicon-time"></i>
                                            </span>
                                            <input type="text" name="jam_berangkat" class="form-control">
                                        </div>
                                    </div>
                                </div>
								<div class="form-group">
                                    <label for="inputStandard" class="col-lg-3 control-label">Harga Sewa</label>
                                    <div class="col-lg-8">
                                        <div class="bs-component">
                                            <input type="text" id="harga_sewa" name="harga_sewa" class="form-control"
                                                    value="<?php echo $harga_sewa;?>">
                                        </div>
                                    </div>
                                </div>
                        <!-- /Wizard -->
						 <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>">
					<button type="submit" class="btn btn-primary">Simpan</button>
                    </form>
					<a href="order.php"><button class="btn btn-primary">Back</button></a>
                    <!-- /Form -->

                <!-- /Spec Form -->
                        </div>
						
                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">

                </div>
            </div>

        </section>
        <!-- /Content -->

    </section>


</div>
<!-- /Body Wrap  -->


<!-- Scripts -->

<!-- jQuery -->
<script src="assets/js/jquery/jquery-1.12.3.min.js"></script>
<script src="assets/js/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- AnimatedSVGIcons -->
<script src="assets/fonts/animatedsvgicons/js/snap.svg-min.js"></script>
<script src="assets/fonts/animatedsvgicons/js/svgicons-config.js"></script>
<script src="assets/fonts/animatedsvgicons/js/svgicons.js"></script>
<script src="assets/fonts/animatedsvgicons/js/svgicons-init.js"></script>

<!-- HighCharts Plugin -->
<script src="assets/js/plugins/highcharts/highcharts.js"></script>

<!-- Scroll -->
<script src="assets/js/utility/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.concat.min.js"></script>

<!-- Theme Scripts -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script src="assets/js/demo/widgets_sidebar.js"></script>
<script src="assets/js/pages/dashboard_init.js"></script>

<!-- Scripts -->

<!-- jQuery -->
<script src="assets/js/jquery/jquery-1.12.3.min.js"></script>
<script src="assets/js/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- AnimatedSVGIcons -->
<script src="assets/fonts/animatedsvgicons/js/snap.svg-min.js"></script>
<script src="assets/fonts/animatedsvgicons/js/svgicons-config.js"></script>
<script src="assets/fonts/animatedsvgicons/js/svgicons.js"></script>
<script src="assets/fonts/animatedsvgicons/js/svgicons-init.js"></script>

<!-- Scroll -->
<script src="assets/js/utility/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.concat.min.js"></script>

<!-- Mixitup -->


<!-- Summernote -->



<!-- HighCharts Plugin -->
<script src="assets/js/plugins/highcharts/highcharts.js"></script>

<!-- Highlight JS -->


<!-- Date/Month - Pickers -->
<script src="assets/js/plugins/globalize/globalize.min.js"></script>
<script src="assets/js/plugins/moment/moment.min.js"></script>
<script src="assets/js/plugins/datepicker/js/bootstrap-datetimepicker.min.js"></script>


<!-- Magnific Popup Plugin -->


<!-- FullCalendar Plugin -->



<!-- Plugins -->














<!-- Google Map API -->





<!-- Jvectormap JS -->




<!-- Datatables JS -->





<!-- FooTable JS -->



<!-- Validate JS -->



<!-- BS Dual Listbox JS -->
<script src="assets/js/plugins/duallistbox/jquery.bootstrap-duallistbox.min.js"></script>

<!-- Bootstrap Maxlength JS -->
<script src="assets/js/plugins/maxlength/bootstrap-maxlength.min.js"></script>

<!-- Select2 JS -->
<script src="assets/js/plugins/select2/select2.min.js"></script>

<!-- Typeahead JS -->
<script src="assets/js/plugins/typeahead/typeahead.bundle.min.js"></script>

<!-- TagManager JS -->
<script src="assets/js/plugins/tagmanager/tagmanager.js"></script>

<!-- DateRange JS -->
<script src="assets/js/plugins/daterange/daterangepicker.min.js"></script>

<!-- BS Colorpicker JS -->
<script src="assets/js/plugins/colorpicker/js/bootstrap-colorpicker.min.js"></script>

<!-- MaskedInput JS -->
<script src="assets/js/plugins/jquerymask/jquery.maskedinput.min.js"></script>

<!-- Slick Slider JS -->


<!-- MarkDown JS -->




<!-- X-edit CSS -->



<script src="assets/js/plugins/typeahead/typeahead.bundle.min.js"></script>


<!-- Dropzone JS -->


<!-- Cropper JS -->


<!-- Zoom JS -->


<!-- Nestable JS -->


<!-- PNotify JS -->


<!-- Fancytree JSs -->







<!-- Ladda JS -->


<!-- NProgress JS -->


<!-- Countdown JS -->



<!-- CanvasBG JS -->


<!-- Theme Scripts -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script src="assets/js/demo/widgets_sidebar.js"></script>

























<script src="assets/js/pages/user-forms-additional-inputs.js"></script>

















<!-- /Scripts -->
<!-- /Scripts -->

</body>


<!-- Mirrored from admink.themerex.net/basic-profile.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 05 Jul 2017 05:08:47 GMT -->
</html>