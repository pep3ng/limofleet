<?php include "session_cek.php" ?>
<html>
<?php include "header_user.php"; ?>
<link href="jquery-ui-1.11.4/smoothness/jquery-ui.css" rel="stylesheet" />
<link href="jquery-ui-1.11.4/jquery-ui.theme.css" rel="stylesheet" />
    <!-- Main Wrapper -->
    <section id="content_wrapper">

        <!-- Content -->
        <section id="content" class="animated fadeIn">

            <div class="row">
                <div class="col-md-12 mb40">

                    <div class="tab-block">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab1" data-toggle="tab">Order</a>
                            </li>
                            <li>
                                <a href="#tab2" data-toggle="tab">Status Order</a>
                            </li>
							<li>
                                <a href="#tab3" data-toggle="tab">History Order</a>
                            </li>
                        </ul>
                        <div class="tab-content p30">
                            <div id="tab1" class="tab-pane active">
                    <div class="panel">
                        <div class="panel-heading">
                            <span class="panel-title">Order</span>
                        </div>
                        <div class="panel-body">
					
						 <form class="form-horizontal" method="post" action="order_user.php">
						 <div class="form-group">
                                    <label for="inputStandard" class="col-lg-3 control-label">Pilih Vendor:</label>
                                    <div class="col-lg-8">
                                        <div class="bs-component">
                                            <select name="fid_perusahaan" class="form-control" onchange="submit()">
													<option value="0">-Pilih Vendor-</option>
													<?php 
													include "db.php";
													$sql = mysqli_query($koneksi,"SELECT * from perusahaan") or die("error sql");
													while($row = mysqli_fetch_assoc($sql)){
													?>
													
													<option value="<?php echo $row['id_perusahaan'];?>"<?php if ( $row['id_perusahaan']==$_POST['fid_perusahaan'] ){echo 'selected="selected"';} else{} ?>><?php echo $row['perusahaan']; ?></option>
													<?php } ?>
													</select>
                                        </div>
                                    </div>
                                </div>
								</form>
									 <div class="allcp-form mw1000 center-block">
						<form class="form-horizontal" method="post" action="simpan_order.php">
									<div class="form-group">
                                    <label for="inputStandard" class="col-lg-3 control-label">Pilih Kendaraan:</label>
                                    <div class="col-lg-8">
                                        <div class="bs-component">
                                            <select name="fid_merk" class="form-control">
													<option value="0">-Pilih Kendaraan-</option>
													<?php 
													$fid_user=$_SESSION['fid_user'];
													$fid_perusahaan=$_POST['fid_perusahaan'];
													$sql = mysqli_query($koneksi,"SELECT * from merk_kendaraan where merk_kendaraan.fid_perusahaan='$fid_perusahaan'") or die("error sql");
													while($row = mysqli_fetch_assoc($sql)){
													?>
													
													<option value="<?php echo $row['id_merk'];?>"><?php echo $row['produsen_kendaraan'];echo "&nbsp"; echo $row['merk_kendaraan']; ?></option>
													<?php } ?>
													</select>
                                        </div>
                                    </div>
                                </div>
                               <div class="form-group">
                                    <label for="inputStandard" class="col-lg-3 control-label">Tanggal Berangkat</label>
                                    <div class="col-lg-8">
                                        <label for="datepicker1" class="field prepend-icon">
                                                <input type="text" id="tanggal" name="tanggal_berangkat"
                                                       class="gui-input"
                                                       placeholder="Tanggal Berangkat">
                                                <span class="field-icon">
                                                    <i class="fa fa-calendar"></i>
                                                </span>
                                            </label>
                                    </div>
									<label for="inputStandard" class="col-lg-3 control-label">Tanggal Datang</label>
                                    <div class="col-lg-8">
                                        <label for="datepicker1" class="field prepend-icon">
                                                <input type="text" id="tanggal2" name="tanggal_datang"
                                                       class="gui-input"
                                                       placeholder="Tanggal Datang">
                                                <span class="field-icon">
                                                    <i class="fa fa-calendar"></i>
                                                </span>
                                            </label>
                                    </div>
                                </div>
								<div class="form-group">
                                    <label for="inputStandard" class="col-lg-3 control-label">Uang Muka</label>
                                    <div class="col-lg-8">
                                        <div class="bs-component">
                                            <input type="text" id="uang_muka" name="uang_muka" class="form-control"
                                                   placeholder="Uang Muka">
                                        </div>
                                    </div>
                                </div>
								<div class="form-group">
                                    <label for="inputStandard" class="col-lg-3 control-label">Jenis Sewa:</label>
                                    <div class="col-lg-8">
                                        <div class="bs-component">
                                            <select name="status_sopir" class="form-control">
													<option value="0">-Jenis Sewa-</option>
													<option value="1">Sopir</option>
													<option value="2">Lepas Kunci</option>
											</select>
                                        </div>
                                    </div>
                                </div>

                        <!-- /Wizard -->
						<input type="hidden" name="fid_perusahaan" value="<?php echo $fid_perusahaan; ?>">
						<input type="hidden" name="fid_user" value="<?php echo $fid_user; ?>">
					<button type="submit" class="btn btn-primary">Simpan</button>
                    </form>
					</div>
                        </div>
                    </div>
                            </div>
                            <div id="tab2" class="tab-pane">
                                <div class="panel" id="spy6">
                    <div class="panel-heading">
                        <span class="panel-title pn">Status Order</span>

                    </div>
                    <div class="panel-body pn">
                       <div class="table-responsive">
                            <div class="bs-component">
                                <table class="table table-striped btn-gradient-grey">
								<thead>
                                    <tr>
                                        <th>No Order</th>
                                        <th>Kendaraan</th>
                                        <th>Uang Muka</th>
                                        <th>Tanggal Berangkat</th>
                                        <th>Tanggal Datang</th>
                                        <th>Status Order</th>
                                    </tr>
                                    </thead>
                                    <tbody>
									<?php 
									$i=0;
									include "db.php";
									$q = mysqli_query($koneksi,"select * from data_order,merk_kendaraan
									WHERE data_order.fid_merk=merk_kendaraan.id_merk
									AND data_order.fid_user='$fid_user'"); 
									while ($row = mysqli_fetch_array($q))
									{
									?>
									 <tr>
                                        <td><?php echo $row['id_order']; ?></td>
                                        <td><?php echo $row['merk_kendaraan']; ?></td>
                                        <td><?php echo $row['uang_muka']; ?></td>
                                        <td><?php echo $row['tanggal_berangkat']; ?></td>
                                        <td><?php echo $row['tanggal_datang']; ?></td>
                                        <td>
										<?php
											if($row['status_order']==1){
												echo "Pending";
											}
											elseif($row['status_order']==2){
												echo "Aktif";
											}
											else{
												echo "Selesai";
											}
										?>
										</td>
                                    </tr>
									<?php 
									$i++;
									}
									?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                            </div>
							        <div id="tab3" class="tab-pane">
                                <div class="panel" id="spy6">
                    <div class="panel-heading">
                        <span class="panel-title pn">Selesai</span>

                    </div>
                    <div class="panel-body pn">
                        <div class="table-responsive">
                            <div class="bs-component">
                                <table class="table table-striped btn-gradient-grey">
								<thead>
                                    <tr>
                                        <th>No Order</th>
                                        <th>Nopol Kendaraan</th>
                                        <th>Merk Kendaraan</th>
                                        <th>Tanggal Berangkat</th>
                                        <th>Tanggal Datang</th>
                                        <th>Status Order</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
									<?php 
									$i=0;
									include "db.php";
									$q = mysqli_query($koneksi,"select * from data_order,kendaraan,merk_kendaraan
									WHERE data_order.fid_kendaraan=kendaraan.id_kendaraan
									AND kendaraan.fid_merk=merk_kendaraan.id_merk
									AND data_order.status_order=3
									AND data_order.fid_perusahaan='$fid_perusahaan'"); 
									while ($row = mysqli_fetch_array($q))
									{
									?>
									 <tr>
                                        <td><?php echo $row['id_order']; ?></td>
                                        <td><?php echo $row['nopol_kendaraan']; ?></td>
                                        <td><?php echo $row['merk_kendaraan']; ?></td>
                                        <td><?php echo $row['tanggal_berangkat']; ?></td>
                                        <td><?php echo $row['tanggal_datang']; ?></td>
                                        <td>Selesai</td>
										<td>
								<a href="input_service.php?id=<?php echo $row['id_order'];?>"><span class="glyphicon glyphicon glyphicon-cog"></span></a>/ 
								<a href="input_cleaning.php?id=<?php echo $row['id_order'];?>"><span class="glyphicon glyphicon glyphicon-tint"></span></a></td>
                                    </tr>
									<?php 
									$i++;
									}
									?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">

                </div>
            </div>

        </section>
        <!-- /Content -->

    </section>


</div>
<!-- /Body Wrap  -->


<!-- Scripts -->

<!-- jQuery -->
<script src="assets/js/jquery/jquery-1.12.3.min.js"></script>
<script src="assets/js/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- AnimatedSVGIcons -->
<script src="assets/fonts/animatedsvgicons/js/snap.svg-min.js"></script>
<script src="assets/fonts/animatedsvgicons/js/svgicons-config.js"></script>
<script src="assets/fonts/animatedsvgicons/js/svgicons.js"></script>
<script src="assets/fonts/animatedsvgicons/js/svgicons-init.js"></script>

<!-- HighCharts Plugin -->
<script src="assets/js/plugins/highcharts/highcharts.js"></script>

<!-- Scroll -->
<script src="assets/js/utility/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.concat.min.js"></script>

<!-- Theme Scripts -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/main.js"></script>
<script src="assets/js/demo/widgets_sidebar.js"></script>
<script src="assets/js/pages/dashboard_init.js"></script>


<!-- jQuery -->
<script src="assets/js/jquery/jquery-1.12.3.min.js"></script>
<script src="assets/js/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- AnimatedSVGIcons -->
<script src="assets/fonts/animatedsvgicons/js/snap.svg-min.js"></script>
<script src="assets/fonts/animatedsvgicons/js/svgicons-config.js"></script>
<script src="assets/fonts/animatedsvgicons/js/svgicons.js"></script>
<script src="assets/fonts/animatedsvgicons/js/svgicons-init.js"></script>

<!-- Scroll -->
<script src="assets/js/utility/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.concat.min.js"></script>

<!-- Mixitup -->


<!-- Summernote -->



<!-- HighCharts Plugin -->
<script src="assets/js/plugins/highcharts/highcharts.js"></script>

<!-- Highlight JS -->


<!-- Date/Month - Pickers -->
<script src="assets/allcp/forms/js/jquery-ui-monthpicker.min.js"></script>
<script src="assets/allcp/forms/js/jquery-ui-datepicker.min.js"></script>
<script src="assets/allcp/forms/js/jquery.spectrum.min.js"></script>
<script src="assets/allcp/forms/js/jquery.stepper.min.js"></script>

<!-- Magnific Popup Plugin -->


<!-- FullCalendar Plugin -->



<!-- Plugins -->














<!-- Google Map API -->





<!-- Jvectormap JS -->




<!-- Datatables JS -->





<!-- FooTable JS -->



<!-- Validate JS -->



<!-- BS Dual Listbox JS -->


<!-- Bootstrap Maxlength JS -->


<!-- Select2 JS -->


<!-- Typeahead JS -->


<!-- TagManager JS -->


<!-- DateRange JS -->


<!-- BS Colorpicker JS -->


<!-- MaskedInput JS -->


<!-- Slick Slider JS -->


<!-- MarkDown JS -->




<!-- X-edit CSS -->






<!-- Dropzone JS -->


<!-- Cropper JS -->


<!-- Zoom JS -->


<!-- Nestable JS -->


<!-- PNotify JS -->


<!-- Fancytree JSs -->







<!-- Ladda JS -->


<!-- NProgress JS -->


<!-- Countdown JS -->



<!-- CanvasBG JS -->


<!-- Theme Scripts -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script src="assets/js/demo/widgets_sidebar.js"></script>









<script src="jquery-ui-1.11.4/jquery.min.js"></script>
<script src="jquery-ui-1.11.4/jquery-ui.js"></script>
<script src="jquery-ui-1.11.4/jquery-ui.min.js"></script>


	<script>
			$(document).ready(function(){
				$("#tanggal").datepicker({
				})
			})
		</script>	
	<script>
			$(document).ready(function(){
				$("#tanggal2").datepicker({
				})
			})
		</script>	



<script src="assets/js/pages/forms-widgets.js"></script>

</body>


<!-- Mirrored from admink.themerex.net/basic-profile.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 05 Jul 2017 05:08:47 GMT -->
</html>