<?php include "session_cek.php" ?>
<html>
<?php include "header_admin.php"; ?>

    <!-- Main Wrapper -->
    <section id="content_wrapper">

        <!-- Content -->
        <section id="content" class="animated fadeIn">

            <div class="row">
                <div class="col-md-12 mb40">

                    <div class="tab-block">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab1" data-toggle="tab">Edit Harga Sewa</a>
                            </li>
                        </ul>
                        <div class="tab-content p30">
                            <div id="tab1" class="tab-pane active">
                    <div class="panel">
                        <div class="panel-heading">
                            <span class="panel-title">Edit Harga Sewa</span>
                        </div>
                        <div class="panel-body">
					<?php 
						include ("db.php");
						$id=$_GET['id'];
						$sql = mysqli_query($koneksi,"SELECT * FROM harga_sewa where id_harga_sewa='$id'") or die("error sql");
						while($row = mysqli_fetch_assoc($sql)){
							$harga_sewa12=$row['harga_sewa12'];
							$harga_sewa24=$row['harga_sewa24'];
						}
						?> 
                   <form class="form-horizontal" method="post" action="update_harga_sewa.php">

                               <div class="form-group">
                                    <label for="inputStandard" class="col-lg-3 control-label">Harga Sewa 12 Jam</label>
                                    <div class="col-lg-8">
                                        <div class="bs-component">
                                            <input type="text" id="harga_sewa12" name="harga_sewa12" class="form-control"
                                                   value="<?php echo $harga_sewa12; ?>">
                                        </div>
                                    </div>
                                </div>
								<div class="form-group">
                                    <label for="inputStandard" class="col-lg-3 control-label">Harga 24 Jam</label>
                                    <div class="col-lg-8">
                                        <div class="bs-component">
                                            <input type="text" id="harga_sewa24" name="harga_sewa24" class="form-control"
                                                   value="<?php echo $harga_sewa24; ?>">
                                        </div>
                                    </div>
                                </div>
					 <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>">
                        <!-- /Wizard -->
					<button type="submit" class="btn btn-primary">Update</button>
                    </form>
					<a href="kendaraan.php"><button class="btn btn-primary">Back</button></a>
                    <!-- /Form -->

                <!-- /Spec Form -->
                        </div>
						
                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">

                </div>
            </div>

        </section>
        <!-- /Content -->

    </section>


</div>
<!-- /Body Wrap  -->


<!-- Scripts -->

<!-- jQuery -->
<script src="assets/js/jquery/jquery-1.12.3.min.js"></script>
<script src="assets/js/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- AnimatedSVGIcons -->
<script src="assets/fonts/animatedsvgicons/js/snap.svg-min.js"></script>
<script src="assets/fonts/animatedsvgicons/js/svgicons-config.js"></script>
<script src="assets/fonts/animatedsvgicons/js/svgicons.js"></script>
<script src="assets/fonts/animatedsvgicons/js/svgicons-init.js"></script>

<!-- HighCharts Plugin -->
<script src="assets/js/plugins/highcharts/highcharts.js"></script>

<!-- Scroll -->
<script src="assets/js/utility/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.concat.min.js"></script>

<!-- Theme Scripts -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script src="assets/js/demo/widgets_sidebar.js"></script>
<script src="assets/js/pages/dashboard_init.js"></script>


<!-- /Scripts -->

</body>


<!-- Mirrored from admink.themerex.net/basic-profile.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 05 Jul 2017 05:08:47 GMT -->
</html>