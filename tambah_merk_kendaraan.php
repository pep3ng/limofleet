<?php include "session_cek.php" ?>
<html>
<?php include "header_admin.php"; ?>

    <!-- Main Wrapper -->
    <section id="content_wrapper">

        <!-- Content -->
        <section id="content" class="animated fadeIn">

            <div class="row">
                <div class="col-md-12 mb40">

                    <div class="tab-block">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab1" data-toggle="tab">Tambah Merk Kendaraan</a>
                            </li>
                        </ul>
                        <div class="tab-content p30">
                            <div id="tab1" class="tab-pane active">
                    <div class="panel">
                        <div class="panel-heading">
                            <span class="panel-title">Tambah Merk Kendaraan</span>
                        </div>
                        <div class="panel-body">
					<?php 
					$fid_perusahaan=$_SESSION['fid_perusahaan'];
					?>
                   <form class="form-horizontal" method="post" action="simpan_merk_kendaraan.php">

                               <div class="form-group">
                                    <label for="inputStandard" class="col-lg-3 control-label">Produsen Kendaraan</label>
                                    <div class="col-lg-8">
                                        <div class="bs-component">
                                            <input type="text" id="produsen_kendaraan" name="produsen_kendaraan" class="form-control"
                                                   placeholder="Produsen Kendaraan">
                                        </div>
                                    </div>
                                </div>
								<div class="form-group">
                                    <label for="inputStandard" class="col-lg-3 control-label">Merk Kendaraan</label>
                                    <div class="col-lg-8">
                                        <div class="bs-component">
                                            <input type="text" id="merk_kendaraan" name="merk_kendaraan" class="form-control"
                                                   placeholder="Merk Kendaraan">
                                        </div>
                                    </div>
                                </div>

                        <!-- /Wizard -->
						<input type="hidden" name="fid_perusahaan" value="<?php echo $fid_perusahaan; ?>">
					<button type="submit" class="btn btn-primary">Simpan</button>
                    </form>
					<a href="kendaraan.php"><button class="btn btn-primary">Back</button></a>
                    <!-- /Form -->

                <!-- /Spec Form -->
                        </div>
						
                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">

                </div>
            </div>

        </section>
        <!-- /Content -->

    </section>


</div>
<!-- /Body Wrap  -->


<!-- Scripts -->

<!-- jQuery -->
<script src="assets/js/jquery/jquery-1.12.3.min.js"></script>
<script src="assets/js/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- AnimatedSVGIcons -->
<script src="assets/fonts/animatedsvgicons/js/snap.svg-min.js"></script>
<script src="assets/fonts/animatedsvgicons/js/svgicons-config.js"></script>
<script src="assets/fonts/animatedsvgicons/js/svgicons.js"></script>
<script src="assets/fonts/animatedsvgicons/js/svgicons-init.js"></script>

<!-- HighCharts Plugin -->
<script src="assets/js/plugins/highcharts/highcharts.js"></script>

<!-- Scroll -->
<script src="assets/js/utility/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.concat.min.js"></script>

<!-- Theme Scripts -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script src="assets/js/demo/widgets_sidebar.js"></script>
<script src="assets/js/pages/dashboard_init.js"></script>


<!-- /Scripts -->

</body>


<!-- Mirrored from admink.themerex.net/basic-profile.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 05 Jul 2017 05:08:47 GMT -->
</html>